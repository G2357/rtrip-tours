<!------------------------------------------------------------------------------
|Objetivo: Parte de la aplicacion que se mantendra fija aun con el cambio de   |
|pantalla, es el encabezado de la pagina                                       |
|Autor original:                           Fecha: 06/03/2017                   |
-------------------------------------------------------------------------------->
<!DOCTYPE html>

<?php
//Se incluyen los archivos del cabezal, menu y pie de pagina
include '/_shared/head.php';
include '/_shared/menu.php';
?>

<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="../images/favicon.ico"> <!--Las librerias de metadatos deben de estar ordenados de como estan plasmados en las tres lineas anteriores-->
        <title>RTrip Tours | Página Principal </title>
        <!-- Bootstrap core CSS -->
        <link href="http://localhost/RTripTours/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="http://localhost/RTripTours/CSS/Estilos/cover.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script src="http://localhost/RTripTours/lib/bootstrap/js/bootstrap.min.js"></script>
    </head>
</html>
<br>
<br>
<br>
<br>
<br>
<br>
<?php
include '/_shared/footer.php';
?>
