﻿<!DOCTYPE html>
<?php
//Se incluyen los archivos del cabezal, menu y pie de pagina
//include '/head.php';
include '/head.php';
?>

<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="../images/favicon.ico"> <!--Las librerias de metadatos deben de estar ordenados de como estan plasmados en las tres lineas anteriores-->
        <title>RTrip Tours | Políticas </title>
        <!-- Bootstrap core CSS -->
        <link href="http://localhost/RTripTours/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="http://localhost/RTripTours/CSS/Estilos/cover.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script src="http://localhost/RTripTours/lib/bootstrap/js/bootstrap.min.js"></script>

        <link rel="icon" href="../../favicon.ico">

        <title>Políticas</title>

        <!-- Bootstrap core CSS -->
        <link href="http://localhost/RTripTours/Librerias/bootstrap-3.3.6/docs/dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <link href="http://localhost/RTripTours/Librerias/bootstrap-3.3.6/docs/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="http://localhost/RTripTours/CSS/Estilos/cover.css" rel="stylesheet">

        <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
        <script src="http://localhost/RTripTours/Librerias/bootstrap-3.3.6/docs/assets/js/ie-emulation-modes-warning.js"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>

        <div class="site-wrapper">

            <div class="site-wrapper-inner4">

                <div class="cover-container">

                    <!--<div class="masthead clearfix">
                      <div class="inner">
                        <h3 class="masthead-brand">ViajAndo MX</h3>
                        <nav>
                          <ul class="nav masthead-nav">
                            <li class="active"><a href="#">Inicio</a></li>
                            <li><a href="#">Acerca de..</a></li>
                            <li><a href="#">Galeria</a></li>
                          </ul>
                        </nav>
                      </div>|
                    </div> -->

                    <div class="inner cover">
                        <h1 class="cover-heading ">Políticas.</h1><!--en esta parte empezare a escribir las politicas 
                            
                            Nota: cada parrafo  tendra que separarse con  <p  </p>-->
                        <h2>Políticas de contratación </h2>
                        <p> Todo empleado nuevo será sometido a un período probatorio. Este será de dos semanas para el personal de los niveles de baja jerarquía y 6 semanas, para los niveles jerárquicos más altos. Al finalizar este período el responsable de la Coordinación Administrativa de inmediato deberá hacer la evaluación y recomendará o no su contratación, a la Dirección General para fines de confirmación en el cargo. </p>
                        <h2>Políticas de Servicio</h2>
                        <p> RTrip Tours S.A única y exclusivamente es la empresa intermediaria entre el usuario de este paquete y las entidades o personas que intervienen en brindar los servicios incluidos en el mismo, como son: hoteles, restaurantes, transportación terrestre, línea aérea, etc. no se hace responsable por daños causados al usuario de este servicio, tales como accidentes, robos, así como cualquier daño, perjuicio o incidente que se le ocasione en la contratación de este paquete.  </p>
                        <p> El usuario deberá expresamente acatar los reglamentos y condiciones establecidas por los prestadores de servicios contratados, eximiendo a RTrip Tours S.A de cualquier responsabilidad derivada de su incumplimiento. </p>
                        <p> Las empresas participantes de este paquete y RTrip Tours S.A se responsabilizan en forma independiente cada una de ellas, y esta última solo intervendrá como intermediaria para gestionar el reembolso del importe que haya pagado o anticipado el usuario.</p>
                        <p> Cualquier servicio no utilizado voluntariamente no dará derecho a reclamación o reembolso alguno.</p>
                        <p> Si por causas de fuerza mayor o casos fortuitos, los prestadores de los servicios contratados no pudieran proporcionarlos parcial o totalmente, la empresa organizadora sólo gestionará por cuenta del usuario el reembolso del importe que proceda con exclusión de cualquier otro compromiso.</p>
                        <p> Cuando RTrip Tours S.A sea quién cancele, un Viaje o Excursión de cualquier naturaleza, por causas ajenas al turista, estará obligada a reembolsarle la totalidad de los anticipos o pagos que haya efectuado. </p>
                        <p> El hecho de que el cliente entregue un anticipo y/o el pago por el programa que haya elegido, implica su aceptación de las condiciones y responsabilidades contenidas en esta publicación.  </p>
                        <p> RTrip Tours S.A. tendrá el derecho para cancelar cualquier salida si así lo cree conveniente por causas ajenas a la agencia, y se compromete a rembolsar los anticipos o el pago total. </p>
                        <p> Las entradas a los hoteles deberán ser invariablemente el día de llegada de llegada de los pasajeros a su destino. La hora de entrada de los hoteles será prevista a partir de las 15:00 horas y la salida a las 13:00 horas; en caso de que la llegada de los pasajeros sea antes de las 15:00 horas, puede darse el caso de que las habitaciones no sean facilitadas hasta este momento. En los días de salida de cada hotel, los pasajeros deberán abandonar la habitación a la hora señalada por el hotel. Las habitaciones cuentan con una o dos camas matrimoniales, las habitaciones triples son con dos camas matrimoniales.   </p>
                        <p> Los precios publicados son en Pesos mexicanos pagaderos en Moneda Nacional. Los precios publicados están sujetos a cambio sin previo aviso. </p>
                        <p> Las reservaciones deberán ser acompañadas por el pago de un depósito del 50% del importe del viaje, liquidándose el saldo como mínimo 7 días antes de la fecha de salida. Las inscripciones serán válidas sólo cuando sean confirmadas en firme por la agencia organizadora y haya sido liquidado el importe total del viaje. La inscripción en cualquiera de los viajes incluidos en esta publicación, implica la total conformidad a todas y cada una de las condiciones estipuladas. </p>
                        <p> Cuando el pasajero desista voluntariamente del viaje contratado para una fecha específica, la cancelación estará libre de gastos si se efectúa antes de los 25 días de la iniciación del viaje. Entre 24 y 15 días se cobrarán 500 Pesos Mexicanos por expediente, Entre 15 y 10 días se cobrarán gastos del 25%, Entre 10 y 3 días se cobrarán gastos del 50%. Dentro de las 48 horas antes de la salida los gastos serán la totalidad del importe del viaje. </p>
                        <p> Para la salida de los paquetes podrán viajar  como mínimo 2 personas. En los tours convencionales no hay mínimos de personas.  </p>
                        <p> Los paquetes son reembolsables siempre y cuando sean cancelados antes de 25 días de la fecha de salida.  <p/>
                        <p> No se aceptarán cupones terrestres en caso de que pretendan ser utilizados. </p>

                        <div>

                            <hr>
                        </div>

                    </div>
                    <div>
                        <p class="lead">
                            <a href="http://localhost/RTripTours/" class="btn btn-warning">Página Principal</a>
                        </p>
                    </div>
                </div>
                <div class="mastfoot">
                </div>
            </div>
        </div>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script src="http://localhost/RTripTours/Librerias/bootstrap-3.3.6/docs/dist/js/bootstrap.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="http://localhost/RTripTours/Librerias/bootstrap-3.3.6/docs/assets/js/ie10-viewport-bug-workaround.js"></script>
    </body>
</html>
<?php
include './footer.php';
?>