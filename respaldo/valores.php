<!DOCTYPE html>

<?php
//Se incluyen los archivos del cabezal, menu y pie de pagina
//include '/_shared/head.php';
include './head.php';//el menu lo contiene head y el menu contiene solo carrusel se necesita cambiar nombre para que se ajuste al standar

?>


<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="../images/favicon.ico"> <!--Las librerias de metadatos deben de estar ordenados de como estan plasmados en las tres lineas anteriores-->
        <title>RTrip Tours | Valores </title>
        <!-- Bootstrap core CSS -->
        <link href="http://localhost/RTripTours/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="http://localhost/RTripTours/CSS/Estilos/cover.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script src="http://localhost/RTripTours/lib/bootstrap/js/bootstrap.min.js"></script>
    </head>

    <body>

        <div class="site-wrapper">

            <div class="site-wrapper-inner3">

                <div class="cover-container">


                    <div class="inner cover">
                        <h1 class="cover-heading"><b>Valores</b></h1>
                        <p class="lead">
                            <br>
                        <h3>Compromiso: Esforzarse para brindar el mejor servicio y atencion posible a nuestros clientes.;;
                        </h3>
                        <br>
                        <h3>Integridad: Relacionarse con los clientes y personal de una manera auténtica.
                        </h3> 
                        <br>
                        <h3>Calidad: De modo que nuestros servicios sean de excelencia.
                        </h3>  
                        <br>
                        <h3>Honestidad: Orientado tanto para los miembros de la empresa entre sí, como con los clientes. Se promueve la verdad como una herramienta elemental para generar confianza y la credibilidad de la empresa.
                        </h3>  
                        <br>
                        <h3>Confianza: Tener personal capacitado para poder brindar a nuestros clientes el mejor servicio con calidad, hospitalidad y puntualidad.
                        </h3>
                        <br>
                        </p>
                        <p class="lead">
                            <a href="http://localhost/RTripTours/" class="btn btn-warning">Pagina Principal</a>
                        </p>
                    </div>


                </div>

            </div>

        </div>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script src="http://localhost/RTripTours/lib/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>
<?php
include './footer.php';//
?>