<!------------------------------------------------------------------------------
|Objetivo: Esta parte muestra el menu principal de la aplicacion con sus       |
|diferentes submenus                                                           |
|                                         |
-------------------------------------------------------------------------------->

<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            
            <div class="container">
                <div class="carousel-caption">
                    <h1>Vive, Sueña... ya estás ahí.</h1>
                    <p>La compañia numero 1 en viajes</p>
                    <p><a  btn-lg btn-success  class="btn btn-warning" href="http://localhost/RTripTours/views/mantenimiento.php" role="button">Saber más</a></p>
                </div>
            </div>
        </div>
        
        <div class="item">
            
            <div class="container">
                <div class="carousel-caption">
                    <h1>El viaje de sus sueños</h1>
                    <p>esta a un clic de distancia</p>
                    <p><a  btn-lg btn-success class="btn btn-warning" href="http://localhost/RTripTours/views/mantenimiento.php" role="button">Saber más</a></p>
                </div>
            </div>
        </div>
        
    </div>
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Anterior</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Siguiente</span>
    </a>
</div><!-- /.carousel -->
