<?php
/* * *****************************************************************************
 * Objetivo: Esta parte muestra el contenido del pie de pagina que contiene la *
 * informacion de la empresa cuyo contenido se encontrara en todas las paginas *
 * de la aplicacion                                                            *
 * Autor: Yessica                               Fecha: 10/03/2017          *
 * ***************************************************************************** */
?>
<html>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script>
    <script src="http://127.0.0.1/RTripTours/lib/bootstrap/js/bootstrap.min.js"></script>

    <footer id="contenedor-footer">
        <p class="pull-left"><a href="http://localhost/RTripTours/index.php">Ir al inicio</a></p>
        <p class="pull-right">&copy; RTrip Tours 2017 </p>
        <p class="pull-right">&Because; Telefonos: (###) ### ## ## &Because; Correo: -------@------ &Because; Twitter:  &Because; Facebook:  &Because;</p>
        <p class="pull-right">&Because; Direccion:  &Because;</p>
        <p>Derechos reservados por <a href="http://getbootstrap.com">RTripTours</a></p>
    </footer>
</html>