<!------------------------------------------------------------------------------
|Objetivo: Parte de la aplicacion que se mantendra fija aun con el cambio de   |
|pantalla, es el encabezado de la pagina                                       |
|Autor original: Berenice                            Fecha: 06/03/2017         |
-------------------------------------------------------------------------------->
<!DOCTYPE html>
<html lang="es">
    <meta charset="utf8">
    <head>
        <!--script>
            $.ajaxSetup({
                'beforeSend': function (xhr) {
                    xhr.overrideMimeType('text/html; charset=iso-8859-1');
                }
             });
        </script-->
        <link rel="icon" href="http://localhost/RTripTours/images/favicon.ico">
        
        <link href="http://127.0.0.1/RTripTours/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="all" type="text/css">
        <link href="http://127.0.0.1/RTripTours/lib/bootstrap/css/login.css" rel="stylesheet" media="all" type="text/css">
        <link href="http://127.0.0.1/RTripTours/lib/bootstrap/css/estilo.css" rel="stylesheet" media="all" type="text/css">
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBRxC6Y4f-j6nECyHWigtBATtJyXyha-XU&libraries=adsense&sensor=true&language=es"></script>
        <script src="http://127.0.0.1/RTripTours/lib/bootstrap/jquery.min.js"></script>
        <script src="http://127.0.0.1/RTripTours/lib/bootstrap/js/bootstrap.min.js"></script>

        <script src="http://127.0.0.1/RTripTours/lib/bootstrap/jspdf.debug.js"></script>

        <link href="http://127.0.0.1/RTripTours/lib/bootstrap/css/datos_grupo.css" rel="stylesheet" media="all" type="text/css">

        <script type="text/javascript" src="http://127.0.0.1/RTripTours/lib/js/html5.js"></script>
        <script type="text/javascript" src="http://127.0.0.1/RTripTours/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="http://127.0.0.1/RTripTours/lib/js/jquery.twitter.js"></script>
        <script type="text/javascript" src="http://127.0.0.1/RTripTours/lib/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="http://127.0.0.1/RTripTours/lib/js/demos.js"></script>
        <script src="http://127.0.0.1/RTripTours/lib/js/in.js" type="text/javascript"></script>
        

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap core CSS -->
        <link href="http://127.0.0.1/RTripTours/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">


        <!-- Custom styles for this template -->
        <link href="http://127.0.0.1/RTripTours/CSS/Estilos/carousel.css" rel="stylesheet">
		
		<link href="http://127.0.0.1/RTripTours/CSS/galeria.css" rel="stylesheet" media="all" type="text/css">
    </head>
    <script lang="javascript">
        function colapsa(item){
            var li_item="";
            if(item==="help_dd"){
                li_item="hdd";
            }else{
                li_item="wdd";
            }    
            if($(item).attr("aria-expanded")){
                $(item).attr("aria-expanded",false);
                $(li_item).removeClass("open");
            }else{
                $(item).attr("aria-expanded",true);
                $(li_item).addClass("open");
            }
            
        }
    </script>

    <body class="dmaf">

        <div class="navbar-wrapper">
            <div class="container">

                <nav id="cabecera-fija" class="navbar navbar-inverse">
                    <div class="container">
                        <div class="navbar-header">
                            <div id="contenedor-header"><a href="#"></a></div>
                        </div>
                        <div id="navbar" class="navbar-collapse collapse">
                            <ul class="nav navbar-nav" id="menu-posision">
                                <li><a href="http://127.0.0.1/RTripTours/views/mantenimiento.php">Galeria</a></li>
                                <li class="dropdown" id="wdd"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Quienes somos<span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="http://127.0.0.1/RTripTours/_shared/mision.php">Misión</a></li>
                                        <li><a href="http://127.0.0.1/RTripTours/_shared/vision.php">Visión</a></li>
                                        <li><a href="http://127.0.0.1/RTripTours/_shared/valores.php">Valores</a></li>
                                        <li role="separator" class="divider"></li>
                                        <li class="dropdown-header"></li>
                                        <li><a href="http://127.0.0.1/RTripTours/_shared/politicas.php">Políticas</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown" id="hdd"> <a id="help_dd" href="#" onclick="javascript:colapsa(this.id)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Ayuda<span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="http://127.0.0.1/RTripTours/_shared/acerca_de.php">Acerca de</a></li>
                                        <li role="separator" class="divider"></li>
                                        <li class="dropdown-header"></li>
                                        <li><a href="#">Ayuda</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>

            </div>
        </div>




