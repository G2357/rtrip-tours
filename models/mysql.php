<?php


class Mysql {

    private $_conexion; //es una variable privada con la nomenclatura en _ 

    public function __construct() { // se genera el contrustor para al inicar esto se haga la conexion y se check y guarde en la variable conexion
        $host = "127.0.0.1";
        $user = "root";
        $pass = "";
        $bd = "rtripdb";
        $port = "3306";
        //guarda la conexion de mysql hace referencia a variables privadas
        $this->_conexion = mysqli_connect($host, $user, $pass, $bd, $port);
        //mysql_set_charset($this->_conexion,'utf8');
    }

    public function query($sql) {
        $result = mysqli_query($this->_conexion, $sql);
        return $result;
    }

    // fin de la clase
    
}
?>