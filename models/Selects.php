<?php

/*******************************************************************************
 * Objetivo: ALmacenar todas las consultas de tipo Select de la BD             *
 * Autores: Aldo, Yessica, Diego                    Fecha: 07/03/2017          *
 *******************************************************************************/

session_start();
require_once("mysql.php");

class Selects extends Mysql {

    /**
     * Funcion que retorna los municipios en relacionados a un estado por nombre
     * @param String $estado
     * @return SQL
     */
    public function consulta_municipios($estado) {
        
        $sql = "SELECT estados.nombre,municipios.id_municipio,municipios.nombre,"
                . "municipios.nombre_completo "
                . "FROM estados,estados_municipios,municipios "
                . "WHERE estados.id_estado=estados_municipios.id_estado "
                . "and estados_municipios.id_municipio=municipios.id_municipio "
                . "and estados.nombre='$estado'; ";

        return $this->query($sql);
    }

    public function consulta_sitio_relacion_normal($clima, $actividad, $sitio) {


        $sql = " SELECT DISTINCT lugares_turisticos.* ,(municipios.nombre) as municipio, "
                . "(estados.nombre) as estado "
                . "FROM estados_municipios, estados, "
                . "est_mun_act_rec, actividades_recreativas, municipios, est_mun_clim, "
                . "climas, est_mun_lug.id_municipio, lugares_turisticos "
                . "WHERE actividades_recreativas.id_actividad=est_mun_act_rec.id_actividad "
                . "and estados.id_estado=est_mun_act_rec.id_estado "
                . "municipios.id_municipio=est_mun_act_rec.id_municipio "
                . "and lugares_turisticos.id_lugar=est_mun_lug.id_lugar "
                . "and estados.id_estado=est_mun_lug.id_estado "
                . "and municipios.id_municipio=est_mun_lug.id_municipio "
                . "and municipios.id_municipio=est_mun_clim.id_clima "
                . "and climas.id_clima=est_mun_clim.id_clima "
                . "and estados.id_estado=estados_municipios.id_estado "
                . "and estados_municipios.id_municipio=municipios.id_municipio "
                . "and actividades_recreativas.nombre='$actividad' "
                . "and climas.nombre_corto='$clima' "
                . "order by lugares_turisticos.id_lugar "
                . "and lugares_turisticos.nombre='$sitio' ;";

        return $this->query($sql);
    }

    public function busca_sitio_relacion_normal($clima, $actividad) {


        $sql = " SELECT DISTINCT lugares_turisticos.* ,(municipios.nombre_completo) as municipio, "
                . "(estados.nombre_completo) as estado "
                . "FROM estados_municipios, estados, "
                . "est_mun_act_rec, actividades_recreativas, municipios, est_mun_clim, "
                . "climas, est_mun_lug.id_municipio, lugares_turisticos "
                . "WHERE actividades_recreativas.id_actividad=est_mun_act_rec.id_actividad "
                . "and estados.id_estado=est_mun_act_rec.id_estado "
                . "municipios.id_municipio=est_mun_act_rec.id_municipio "
                . "and lugares_turisticos.id_lugar=est_mun_lug.id_lugar "
                . "and estados.id_estado=est_mun_lug.id_estado "
                . "and municipios.id_municipio=est_mun_lug.id_municipio "
                . "and estados.id_estado=est_mun_clim.id_estado "
                . "and municipios.id_municipio=est_mun_clim.id_municipio "
                . "and climas.id_clima=est_mun_clim.id_clima "
                . "and estados.id_estado=estados_municipios.id_estado "
                . "and estados_municipios.id_municipio=municipios.id_municipio "
                . "and actividades_recreativas.nombre='$actividad' "
                . "and climas.nombre_corto='$clima' "
                . "order by lugares_turisticos.id_lugar;";


        return $this->query($sql);
    }

    public function busca_sitio_relacion_hotel($clima, $actividad, $clasificacion) {


        $sql = " SELECT DISTINCT lugares_turisticos.* ,(municipios.nombre_completo) as municipio, "
                . "(estados.nombre_completo) as estado "
                . "FROM estados_municipios, estados, "
                . "est_mun_act_rec, categoria_hoteles, actividades_recreativas, "
                . "municipios, est_mun_clim, climas, est_mun_lug.id_municipio, lugares_turisticos, "
                . "hoteles, est_mun_hot "
                . "WHERE actividades_recreativas.id_actividad=est_mun_act_rec.id_actividad "
                . "and estados.id_estado=est_mun_act_rec.id_estado "
                . "municipios.id_municipio=est_mun_act_rec.id_municipio "
                . "and lugares_turisticos.id_lugar=est_mun_lug.id_lugar "
                . "and estados.id_estado=est_mun_lug.id_estado "
                . "and municipios.id_municipio=est_mun_lug.id_municipio "
                . "and estados.id_estado=est_mun_clim.id_estado "
                . "and municipios.id_municipio=est_mun_clim.id_municipio "
                . "and climas.id_clima=est_mun_clim.id_clima "
                . "and estados.id_estado=est_mun_hot.id_estado "
                . "and municipios.id_municipio=est_mun_hot.id_municipio "
                . "and est_mun_hot.id_hotel=hoteles.id_hotel "
                . "and categoria_hoteles.id_categoria=hoteles.id_categoria "
                . "and estados.id_estado=estados_municipios.id_estado "
                . "and estados_municipios.id_municipio=municipios.id_municipio "
                . "and actividades_recreativas.nombre='$actividad' "
                . "and hoteles.id_categoria='$clasificacion' "
                . "and climas.nombre_corto='$clima' "
                . "order by lugares_turisticos.id_lugar;";


        return $this->query($sql);
    }

    public function consultas_sitio_relacion_hotel($clima, $actividad, $clasificacion, $sitio) {


        $sql = "SELECT DISTINCT lugares_turisticos.* ,(municipios.nombre) as municipio, "
                . "(estados.nombre) as estado "
                . "FROM estados_municipios, estados, est_mun_act_rec, "
                . "categoria_hoteles, actividades_recreativas, municipios, est_mun_clim, climas, "
                . "est_mun_lug.id_municipio, lugares_turisticos, hoteles, est_mun_hot "
                . "WHERE actividades_recreativas.id_actividad=est_mun_act_rec.id_actividad "
                . "and estados.id_estado=est_mun_act_rec.id_estado "
                . "municipios.id_municipio=est_mun_act_rec.id_municipio "
                . "and lugares_turisticos.id_lugar=est_mun_lug.id_lugar "
                . "and estados.id_estado=est_mun_lug.id_estado "
                . "and municipios.id_municipio=est_mun_lug.id_municipio "
                . "and estados.id_estado=est_mun_clim.id_estado "
                . "and municipios.id_municipio=est_mun_clim.id_municipio "
                . "and climas.id_clima=est_mun_clim.id_clima "
                . "and estados.id_estado=est_mun_hot.id_estado "
                . "and municipios.id_municipio=est_mun_hot.id_municipio "
                . "and est_mun_hot.id_hotel=hoteles.id_hotel "
                . "and categoria_hoteles.id_categoria=hoteles.id_categoria "
                . "and estados.id_estado=estados_municipios.id_estado "
                . "and estados_municipios.id_municipio=municipios.id_municipio "
                . "and actividades_recreativas.nombre='$actividad' "
                . "and hoteles.id_categoria='$clasificacion' "
                . "and climas.nombre_corto='$clima' "
                . "order by lugares_turisticos.id_lugar "
                . "and lugares_turisticos.nombre='$sitio' ;";


        return $this->query($sql);
    }

    public function busca_clima_relacion($clima, $actividad) {


        $sql = " SELECT DISTINCT climas.id_clima,climas.clima,climas.nombre_corto "
                . "FROM est_mun_act_rec,actividades_recreativas,municipios,"
                . "est_mun_clim,climas,est_mun_lug.id_municipio,lugares_turisticos "
                . "WHERE actividades_recreativas.id_actividad=est_mun_act_rec.id_actividad "
                . "and estados.id_estado=est_mun_act_rec.id_estado "
                . "municipios.id_municipio=est_mun_act_rec.id_municipio "
                . "and lugares_turisticos.id_lugar=est_mun_lug.id_lugar "
                . "and estados.id_estado=est_mun_lug.id_estado "
                . "and municipios.id_municipio=est_mun_lug.id_municipio "
                . "and estados.id_estado=est_mun_clim.id_estado "
                . "and municipios.id_municipio=est_mun_clim.id_municipio "
                . "and climas.id_clima=est_mun_clim.id_clima "
                . "and actividades_recreativas.nombre='$actividad' "
                . "and climas.nombre_corto='$clima'; ";


        return $this->query($sql);
    }

    public function busca_categoria_hoteles($clima, $actividad, $clasificacion) {


        $sql = "SELECT DISTINCT categoria_hoteles.* "
                . "FROM est_mun_act_rec,categoria_hoteles,actividades_recreativas,municipios,"
                . "est_mun_clim,climas,est_mun_lug.id_municipio,lugares_turisticos,hoteles,est_mun_hot "
                . "WHERE actividades_recreativas.id_actividad=est_mun_act_rec.id_actividad "
                . "and estados.id_estado=est_mun_act_rec.id_estado "
                . "municipios.id_municipio=est_mun_act_rec.id_municipio "
                . "and lugares_turisticos.id_lugar=est_mun_lug.id_lugar "
                . "and estados.id_estado=est_mun_lug.id_estado "
                . "and municipios.id_municipio=est_mun_lug.id_municipio "
                . "and estados.id_estado=est_mun_clim.id_estado "
                . "and municipios.id_municipio=est_mun_clim.id_municipio "
                . "and climas.id_clima=est_mun_clim.id_clima "
                . "and actividades_recreativas.nombre='$actividad' "
                . "and climas.nombre_corto='$clima' "
                . "and estados.id_estado=est_mun_hot.id_estado "
                . "and municipios.id_municipio=est_mun_hot.id_municipio "
                . "and est_mun_hot.id_hotel=hoteles.id_hotel "
                . "and categoria_hoteles.id_categoria=hoteles.id_categoria "
                . "and categoria_hoteles.nombre_corto='$clasificacion' ; ";


        return $this->query($sql);
    }

    public function consulta_hoteles_relacion($actividad, $clima) {


        $sql = "SELECT DISTINCT categoria_hoteles.* "
                . "FROM est_mun_act_rec,"
                . "categoria_hoteles,actividades_recreativas,municipios,est_mun_clim,climas,"
                . "est_mun_lug.id_municipio,lugares_turisticos,hoteles,est_mun_hot "
                . "WHERE actividades_recreativas.id_actividad=est_mun_act_rec.id_actividad "
                . "and estados.id_estado=est_mun_act_rec.id_estado "
                . "municipios.id_municipio=est_mun_act_rec.id_municipio "
                . "and lugares_turisticos.id_lugar=est_mun_lug.id_lugar "
                . "and estados.id_estado=est_mun_lug.id_estado "
                . "and municipios.id_municipio=est_mun_lug.id_municipio "
                . "and estados.id_estado=est_mun_clim.id_estado "
                . "and municipios.id_municipio=est_mun_clim.id_municipio "
                . "and climas.id_clima=est_mun_clim.id_clima "
                . "and actividades_recreativas.nombre='$actividad' "
                . "and climas.nombre_corto='$clima' "
                . "and estados.id_estado=est_mun_hot.id_estado "
                . "and municipios.id_municipio=est_mun_hot.id_municipio "
                . "and est_mun_hot.id_hotel=hoteles.id_hotel "
                . "and categoria_hoteles.id_categoria=hoteles.id_categoria ;";


        return $this->query($sql);
    }

    public function consulta_clima_relacion($actividad) {


        $sql = " SELECT DISTINCT climas.id_clima,climas.clima,climas.nombre_corto "
                . "FROM est_mun_act_rec,actividades_recreativas,municipios,est_mun_clim,"
                . "climas,est_mun_lug.id_municipio,lugares_turisticos "
                . "WHERE actividades_recreativas.id_actividad=est_mun_act_rec.id_actividad "
                . "and estados.id_estado=est_mun_act_rec.id_estado "
                . "municipios.id_municipio=est_mun_act_rec.id_municipio "
                . "and lugares_turisticos.id_lugar=est_mun_lug.id_lugar "
                . "and estados.id_estado=est_mun_lug.id_estado "
                . "and municipios.id_municipio=est_mun_lug.id_municipio "
                . "and estados.id_estado=est_mun_clim.id_estado "
                . "and municipios.id_municipio=est_mun_clim.id_municipio "
                . "and climas.id_clima=est_mun_clim.id_clima "
                . "and actividades_recreativas.nombre='$actividad'; ";


        return $this->query($sql);
    }

    public function consulta_actividades_tabla() {


        $sql = "SELECT * FROM actividades ";


        return $this->query($sql);
    }

    public function consulta_lexico($numero) {


        $sql = "SELECT * FROM lexicos WHERE lexicos.id_lexico='$numero'; ";


        return $this->query($sql);
    }

    public function preguntas_agente($pregunta) {


        $sql = "SELECT * FROM rtripdb.preguntas " .
                " WHERE preguntas.id_pregunta='$pregunta' ";


        return $this->query($sql);
    }

    //si buscara una respuesta o resultado a la contestacion del usuario

    public function datos_hotel($hotel, $municipio) {


        $sql = "SELECT hoteles.nombre,hoteles.nombre_completo,hoteles.id_hotel,"
                . "hoteles.ubicacion,hoteles.precio "
                . "FROM municipios, hoteles, est_mun_hot "
                . "WHERE municipios.id_municipio=est_mun_hot.id_municipio "
                . "and est_mun_hot.id_hotel=hoteles.id_hotel "
                . "and municipios.nombre='$municipio' "
                . "and hoteles.nombre='$hotel';";

        return $this->query($sql);
    }

    public function datos_paquete($paquete) {


        $sql = "SELECT * FROM paquetes WHERE nombre='$paquete';";

        return $this->query($sql);
    }

    public function datos_transporte($estado, $transporte) {


        $sql = "SELECT medios_transporte.id_transporte,transportes.nombre,transportes.ciudad,"
                . "transportes.nombre_completo "
                . "FROM medios_transporte,est_mun_med_tran,estados "
                . "WHERE medios_transporte.id_transporte=est_mun_med_tran.id_transporte "
                . "and est_mun_med_tran.id_estado=estados.id_estado "
                . "and est_mun_med_tran.id_municipio=municipios.id_municipio "
                . "and estados.nombre='$estado' "
                . "and medios_transporte.nombre='$transporte';";

        return $this->query($sql);
    }

    public function datos_viaje($estado, $municipio) {


        $sql = "SELECT (estados.nombre_completo) as estado,"
                . "(municipios.nombre_completo) as municipio,climas.clima "
                . "FROM estados,estados_municipios,municipios,est_mun_clim,climas "
                . "WHERE estados.id_estado=estados_municipios.id_estado "
                . "and estados_municipios.id_municipio=municipios.id_municipio "
                . "and municipios.nombre='$municipio' "
                . "and estados.nombre='$estado' "
                . "and estados.id_estado=est_mun_clim.id_estado "
                . "and municipios.id_municipio=est_mun_clim.id_municipio "
                . "and est_mun_clim.id_clima=climas.id_clima ; ";


        return $this->query($sql);
    }

    public function datos_restaurante($restaurante, $municipio) {


        $sql = "SELECT restaurantes.id_restaurante,restaurantes.nombre,"
                . "restaurantes.nombre_completo,restaurantes.direccion "
                . "FROM municipios,est_mun_res,restaurantes "
                . "WHERE estados.id_estado=est_mun_res.id_estado "
                . "municipios.id_municipio=est_mun_res.id_municipio "
                . "and est_mun_res.id_restaurant=restaurantes.id_restaurante "
                . "and municipios.nombre='$municipio' "
                . "and restaurantes.nombre='$restaurante';";


        return $this->query($sql);
    }

    public function respuestas($pregunta) {


        $sql = "SELECT * "
                . "FROM rtripdb.tiene_respuestas "
                . "WHERE tiene_respuestas.id_pregunta='$pregunta'; ";


        return $this->query($sql);
    }

    public function encuentra_resultado_sinonimo_tabla($palabra) {


        $sql = "SELECT * FROM " 
                . "sinonimos,equivalen,palabras "
                . "WHERE sinonimos.id_sinonimo=equivalen.id_sinonimo "
                . "and equivalen.id_palabra=palabras.id_palabra "
                . "and sinonimos.sinonimo='$palabra '; ";


        return $this->query($sql);
    }

    public function busca_actividad($palabra) {


        $sql = "SELECT * "
                . "FROM actividades "
                . "WHERE actividades_recreativas.nombre='$palabra'";


        return $this->query($sql);
    }

    public function encuentra_resultado_sinonimo($palabra, $tabla) {


         $sql = "SELECT * "
                 . "FROM {$tabla}, equivalen, palabras "
                 . "WHERE  sinonimos.id_sinonimo=equivalen.id_sinonimo "
                 . "and equivalen.id_palabra=palabras.id_palabra "
                 . "and $tabla.sinonimo='$palabra '; ";


        return $this->query($sql);
    }

    public function encuentra_paquete($paquete) {

        $sql = "SELECT * FROM paquetes WHERE paquetes.nombre='$paquete'; ";

        return $this->query($sql);
    }

    public function consulta_paquete() {

        $sql = "SELECT * FROM paquetes ; ";

        return $this->query($sql);
    }

    public function consulta_un_aeropuertos($transporte, $estado) {

        $sql = "SELECT DISTINCT medios_transporte.id_transporte,medios_transporte.nombre,"
                . "medios_transporte.ciudad,medios_transporte.nombre_completo "
                . "FROM medios_transporte,est_mun_med_tran,estados "
                . "WHERE medios_transporte.id_transporte=est_mun_med_tran.id_transporte "
                . "and est_mun_med_tran.id_estado=estados.id_estado "
                . "and est_mun_med_tran.id_municipio=municipios.id_municipio "
                . "and estados.nombre='$estado' and  medios_transporte.nombre='$transporte' ;";

        return $this->query($sql);
    }

    public function consulta_aeropuertos($estado) {

        $sql = "SELECT medios_transporte.id_transporte,medios_transportet.nombre,"
                . "transportes.ciudad,medios_transporte.nombre_completo "
                . "FROM medios_transporte,est_mun_med_tran,estados "
                . "WHERE medios_transporte.id_transporte=est_mun_med_tran.id_transporte "
                . "and est_mun_med_tran.id_estado=estados.id_estado "
                . "and est_mun_med_tran.id_municipio=municipios.id_municipio "
                . "and estados.nombre='$estado';";

        return $this->query($sql);
    }

    public function encuentra_sitios_turisticos($sitio, $municipio) {

        $sql = "SELECT DISTINCT lugares_turisticos.nombre,lugares_turisticos.nombre_completo,"
                . "lugares_turisticos.ubicacion,lugares_turisticos.id_lugar "
                . "FROM municipios,est_mun_lug.id_municipio,lugares_turisticos,estados "
                . "WHERE estados.id_estado=est_mun_lug.id_estado "
                . "and municipios.id_municipio=est_mun_lug.id_municipio "
                . "and est_mun_lug.id_lugar=lugares_turisticos.id_lugar "
                . "and estados.id_estado=est_mun_lug.id_estado "
                . "and municipios.id_municipio=est_mun_lug.id_municipio "
                . "and est_mun_lug.id_lugar=lugares_turisticos.id_lugar "
                . "and municipios.nombre='$municipio' "
                . "and lugares_turisticos.nombre='$sitio'; ";

        return $this->query($sql);
    }

    public function encuentra_hotel($hotel, $municipio) {

        $sql = " SELECT hoteles.id_hotel,hoteles.nombre,hoteles.nombre_completo,"
                . "hoteles.ubicacion,hoteles.precio "
                . "FROM municipios,est_mun_hot,hoteles "
                . "WHERE municipios.id_municipio =est_mun_hot.id_municipio "
                . "and est_mun_hot.id_hotel=hoteles.id_hotel "
                . "and municipios.nombre='$municipio' "
                . "and hoteles.nombre='$hotel' ;";

        return $this->query($sql);
    }

    public function encuentra_restaurante($restaunate, $municipio) {

        $sql = "SELECT restaurantes.nombre,restaurantes.nombre_completo,restaurantes.direccion,restaurantes.id_restaurante "
                . "FROM municipios,est_mun_res,restaurantes "
                . "WHERE restaurantes.id_restaurante=est_mun_res.id_restaurant "
                . "and estados.id_estado=est_mun_res.id_estado "
                . "municipios.id_municipio=est_mun_res.id_municipio "
                . "and municipios.nombre='$municipio' "
                . "and restaurantes.nombre='$restaunate ';";

        return $this->query($sql);
    }

    public function encuentra_resultado($palabra, $tabla) {


        $sql = "SELECT * FROM " .
                " $tabla  WHERE   $tabla.nombre='$palabra '; ";


        return $this->query($sql);
    }

    public function encuentra_estado($palabra) {


        $sql = "SELECT * FROM estados WHERE estados.nombre='$palabra' ;";


        return $this->query($sql);
    }

    public function validar_municipio_estado($estado, $municipio) {

        $sql = "SELECT * "
                . "FROM municipios,estados_municipios,estados "
                . "WHERE municipios.id_municipio=estados_municipios.id_municipio "
                . "and estados.id_estado=estados_municipios.id_estado "
                . "and estados.nombre='$estado' "
                . "and municipios.nombre='$municipio' ; ";

        return $this->query($sql);
    }

    public function validar_municipio($municipio) {

        $sql = "SELECT * "
                . "FROM municipios,estados_municipios,estados"
                . "WHERE municipios.id_municipio=estados_municipios.id_municipio "
                . "and estados.id_estado=estados_municipios.id_estado "
                . "and municipios.nombre='$municipio' ; ";

        return $this->query($sql);
    }

    public function consulta_municipio($municipio) {


        $sql = "SELECT DISTINCT * "
                . "FROM est_mun_clim,climas, municipios "
                . "WHERE climas.id_clima=est_mun_clim.id_clima "
                . "and est_mun_clim.id_municipio=municipios.id_municipio "
                . "and  municipios.nombre='$municipio' ;";
        return $this->query($sql);
    }

    public function consulta_estado($estado) {


        $sql = "SELECT DISTINCT * FROM estados WHERE estados.nombre='$municipio';";
        return $this->query($sql);
    }

    public function borrar_chat() {


        $sql = "delete `rtripdb`.`conversaciones` "
                . "FROM conversaciones ; ";

        return $this->query($sql);
    }

    public function consulta_actividades($municipio) {

        $sql = "SELECT DISTINCT lugares_turisticos.id_lugar, lugares_turisticos.nombre, "
                . "lugares_turisticos.nombre_completo,lugares_turisticos.ubicacion "
                . "FROM municipios,est_mun_lug.id_municipio,lugares_turisticos "
                . "WHERE municipios.nombre='$municipio' "
                . "and estados.id_estado=est_mun_lug.id_estado "
                . "and municipios.id_municipio=est_mun_lug.id_municipio "
                . "and est_mun_lug.id_lugar=lugares_turisticos.id_lugar; ";

        return $this->query($sql);
    }

    public function consulta_hoteles($municipio) {

        $sql = " SELECT DISTINCT hoteles.id_hotel,hoteles.nombre,hoteles.nombre_completo,"
                . "hoteles.ubicacion,hoteles.precio "
                . "FROM municipios,est_mun_hot,hoteles "
                . "WHERE municipios.id_municipio=est_mun_hot.id_municipio "
                . "and est_mun_hot.id_hotel=hoteles.id_hotel "
                . "and municipios.nombre='$municipio' ; ";

        return $this->query($sql);
    }

    public function consulta_restaurantes($municipio) {

        $sql = "SELECT  DISTINCT restaurantes.id_restaurante , restaurantes.nombre, "
                . "restaurantes.nombre_completo, restaurantes.direccion "
                . "FROM municipios,est_mun_res,restaurantes "
                . "WHERE restaurantes.id_restaurante=est_mun_res.id_restaurant "
                . "and estados.id_estado=est_mun_res.id_estado "
                . "municipios.id_municipio=est_mun_res.id_municipio "
                . "and municipios.nombre='$municipio';";

        // $sql="SELECT restaurantes.id_restaurante,restaurantes.nombre,restaurantes.nombre_completo,restaurantes.direccion "
        //      ." FROM municipios,est_mun_res,restaurantes Where estados.id_estado=est_mun_res.id_estado "
         //       . "municipios.id_municipio=est_mun_res.id_municipio "
        //    ." and restaurantes.id_restaurante=est_mun_res.id_restaurant and municipios.nombre='$municipio';";


        return $this->query($sql);
    }

    public function consulta_comida($municipio) {

        $sql = " SELECT  DISTINCT comidas_tipicas.id_comida, nombre_comida "
                . "FROM municipios,est_mun_com_tip,comidas_tipicas "
                . "WHERE estados.id_estado=est_mun_com_tip.id_estado "
                . "and municipios.id_municipio=est_mun_com_tip.id_municipio "
                . "and est_mun_com_tip.id_comida=comidas_tipicas.id_comida "
                . "and municipios.nombre='$municipio'; ";

        return $this->query($sql);
    }

    public function consulta_hotele($municipio) {

        $sql = " SELECT DISTINCT hoteles.id_hotel,hoteles.nombre,hoteles.ubicacion,hoteles.precio "
                . "FROM hoteles,est_mun_hot,municipios "
                . "WHERE hoteles.id_hotel=est_mun_hot.id_hotel "
                . "and est_mun_hot.id_municipio=municipios.id_municipio "
                . "and municipios.nombre='$municipio'; ";

        return $this->query($sql);
    }

}
