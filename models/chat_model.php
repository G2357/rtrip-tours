<?php

/*******************************************************************************
 * Objetivo: procedimiento de arranque para cada ejecucion de chat             *
 * Autor: Noel Juan Chavero                         Fecha: 07/03/2017          *
 *******************************************************************************/


//se crea un objeto del metodo converacion
    include("../models/Selects.php");
    include("../models/Inserts.php");
    include("../models/Deletes.php");
    include("../models/Funciones.php");
    $select= new Selects();
    $inserts= new Inserts();
    $deletes= new Deletes();
    $funciones= new Funciones();
//banderas para controlar errores
    $busca_error = 1;
    $activa_consulta = 0;

//se valida que se envien datos por el metodo post
    if (isset($_POST['usuario'])) {

        $_SESSION['limpiar'] = $_SESSION['limpiar'] + 1;

        if ($_SESSION['limpiar'] == 1) {
            $borrar = $deletes->borrar_chat();
            $_SESSION['limpiar'] = 0;
        }
        $valida_datos = $_POST['usuario'];
        //se inserta un nuevo mensaje en el chat
        $Registrar = $inserts->nuevo_mensaje($valida_datos);
        $valida_datos['mensaje'];
        //esto es para hacer algunas pruebas en la versión final esto estará oculto para el usuario
        //se almacena en la variable cadena los datos ingresados por el usuario en el cuadro de texto 
        $cadena = $valida_datos['mensaje'];


        $cadena = $funciones->Quitar_acentos($cadena);
        $arreglo_cadena = explode(" ", $cadena);
        count($arreglo_cadena) - 1;

        // se usa un ciclo para ir validando palabra a palabra que se encuentre en la tabla sinónimos
        $respuesta = $select->respuestas($_SESSION['numero_pregunta']);
        $arreglo_respuesta = mysqli_fetch_array($respuesta);

        //para mover el arreglo hasta encontrar la palabra en el sinónimo
        $cambio_palabra = 0;
        //ciclo que buscara la palabra en el arreglo

        while ($cambio_palabra <= (count($arreglo_cadena) - 1)) {

            if ($_SESSION['numero_pregunta'] == 1) {

                $encuentra_resultado = $select->encuentra_resultado_sinonimo($arreglo_cadena[$cambio_palabra], $arreglo_respuesta['tabla']);
                $arreglo_encuentra_resultado = mysqli_fetch_array($encuentra_resultado);

                //se compara el arregló si tiene datos es que encontró un resultado
                if ($arreglo_encuentra_resultado) {
                    //se valida con el tipo de pregunta que es lo que quiere el usuario y depende de la respuesta 
                    //se otorga una acción

                    if ($arreglo_encuentra_resultado['palabra'] == 'afirmacion') {

                        //con esta sección si encontró los datos va a mostrar la información o la respuesta a dicha pregunta  $Registrar = $inserts->mensaje_agente($arreglo_respuesta['respuesta']);
                        $_SESSION['numero_pregunta'] = 2;
                        $busca_error = 0;
                        $cambio_palabra = count($arreglo_cadena) + 2;
                    } elseif ($arreglo_encuentra_resultado['palabra'] == 'negacion') {
                        //se encontro respuesta
                        $cambio_palabra = count($arreglo_cadena) + 2;


                        $respuesta = $select->consulta_lexico('8');
                        $arreglo_respuesta_lexico = mysqli_fetch_array($respuesta);

                        $Registrar = $inserts->mensaje_agente($arreglo_respuesta_lexico['texto']);

                        $_SESSION['numero_pregunta'] = 16;
                        $busca_error = 0;
                    }
                } else {

                    $cambio_palabra = $cambio_palabra + 1;
                }
            } elseif ($_SESSION['numero_pregunta'] == 2) {



                $encuentra_resultado = $select->encuentra_resultado_sinonimo_tabla($arreglo_cadena[$cambio_palabra], $arreglo_respuesta['tabla']);
                $arreglo_encuentra_resultado = mysqli_fetch_array($encuentra_resultado);
                //se compara el arreglo si si tiene datos es que encontro un resultado

                if ($arreglo_encuentra_resultado['palabra'] == 'negacion') {
                    //se encontro respuesta
                    $cambio_palabra = count($arreglo_cadena) + 2;
                    $_SESSION['numero_pregunta'] = 101;
                    $busca_error = 0;
                    
                    
                    //se cambia a 3 indicando que esta entrando al caso 3 esto para reutilizar secciones de codigo mas adelante
                    //como ejemplo no preguntar si sabe los hoteles que tiene un municipio por que si es caso 3 "el cliente no sabe nada"
                    //no sabe el municipio por lo tanto se muestran direcatemnte estos datos
                    $_SESSION['caso'] = "3";
                } else {

                    //debido a que en esta opcion se permite que directamente el usuario ingrese tanto el estado como el municipio
                    //tiene las 2 opciones o dos recorridos de arreglos
                    $encuentra_resultado_estado = $select->encuentra_resultado($arreglo_cadena[$cambio_palabra], 'estados');
                    $arreglo_encuentra_resultado_estado = mysqli_fetch_array($encuentra_resultado_estado);


                    //se valida que se encontraran los datos y si si se asignan a las variables 
                    if ($arreglo_encuentra_resultado_estado) {
                        $_SESSION['estado'] = $arreglo_encuentra_resultado_estado['nombre'];
                        //al utilizar ya esta cadena se cambia por n para evitar la confucion al ser utilizada 

                        if ($_SESSION['estado']) {
                            $arreglo_cadena[$cambio_palabra] = 'x';
                        }
                        $encuentra_resultado_municipio = $select->encuentra_resultado($arreglo_cadena[$cambio_palabra], 'municipios');
                        $arreglo_encuentra_resultado_municipio = mysqli_fetch_array($encuentra_resultado_municipio);

                        if ($arreglo_encuentra_resultado_municipio) {
                            $_SESSION['municipio'] = $arreglo_encuentra_resultado_municipio['nombre'];
                        }
                    }

                    //se valida si el municipio pertenece al estado seleccionado
                    if ($_SESSION['estado'] and $_SESSION['municipio']) {

                        $valida_datos = $select->validar_municipio_estado($_SESSION['estado'], $_SESSION['municipio']);


                        $arreglo_valida_datos = mysqli_fetch_array($valida_datos);

                        if ($arreglo_valida_datos) {

                            $respuesta = $select->consulta_lexico('16');
                            $arreglo_respuesta_lexico = mysqli_fetch_array($respuesta);
                            $municipio = $arreglo_respuesta_lexico['texto'];

                            $respuesta = $select->consulta_lexico('17');
                            $arreglo_respuesta_lexico = mysqli_fetch_array($respuesta);
                            $clima = $arreglo_respuesta_lexico['texto'];


                            $Registrar = $inserts->mensaje_agente($arreglo_respuesta['respuesta']);
                            $consulta_municipio = $select->consulta_municipio($_SESSION['municipio']);
                            $arreglo_consulta_municipio = mysqli_fetch_array($consulta_municipio);
                            //$mensaje_editado = 'El municipio de ' + $_SESSION['municipio'] + "  Perteneciente al estado de   " + $_SESSION['estado']+ "cuenta con un clima " + $arreglo_consulta_municipio['clima'];
                            $mensaje_editado = ' ' . $municipio . ' ' . $_SESSION['municipio'] . ' ' . $clima . ' ' . $arreglo_consulta_municipio['clima'];
                            $Registrar = $inserts->mensaje_agente($mensaje_editado);
                            //la numeracion cambia puesto que el num de pregunta hace referencia al siguiente
                            //cuestionamiento que se le ara al cliente
                            $_SESSION['numero_pregunta'] = 4;
                            $busca_error = 0;
                            $cambio_palabra = count($arreglo_cadena) + 2;
                        } else {

                            $respuesta = $select->consulta_lexico('18');
                            $arreglo_respuesta_lexico = mysqli_fetch_array($respuesta);
                            $error = $arreglo_respuesta_lexico['texto'];


                            $Registrar = $inserts->mensaje_agente($error);
                            $_SESSION['numero_pregunta'] = 2;
                            $busca_error = 0;
                            $cambio_palabra = count($arreglo_cadena) + 2;
                        }
                    } elseif ($_SESSION['estado'] && $cambio_palabra <= (count($arreglo_cadena) - 1)) {

                        $_SESSION['numero_pregunta'] = 21;
                        $busca_error = 0;
                        $cambio_palabra = count($arreglo_cadena) + 2;
                    } else {



                        $cambio_palabra = $cambio_palabra + 1;
                    }
                }
            } elseif ($_SESSION['numero_pregunta'] == 21) {

                //esta seccion es de sinonimos debe estar en una funcion o archivo para utilizarla varias ocaciones
                if ($arreglo_respuesta['tabla'] == 'sinonimos') {
                    //se manda palabra a palabra en la funcion para encontrar similitudes
                    $encuentra_resultado = $select->encuentra_resultado_sinonimo($arreglo_cadena[$cambio_palabra], $arreglo_respuesta['tabla']);
                    $arreglo_encuentra_resultado = mysqli_fetch_array($encuentra_resultado);

                    //se compara elarreglo si si tiene datos es que encontro un resultado
                    if ($arreglo_encuentra_resultado) {

                        //se valida con el tipo de pregunta que es lo que quiere el usuario y depende de la respuesta 
                        //se otorga una accion
                        if ($arreglo_encuentra_resultado['palabra'] == 'afirmacion') {

                            while ($cambio_palabra <= (count($arreglo_cadena) - 1)) {

                                $encuentra_resultado_municipio = $select->consulta_municipio($arreglo_cadena[$cambio_palabra]);
                                $arreglo_encuentra_resultado_municipio = mysqli_fetch_array($encuentra_resultado_municipio);


                                if ($arreglo_encuentra_resultado_municipio) {
                                    $_SESSION['municipio'] = $arreglo_encuentra_resultado_municipio['nombre'];
                                }
                                if ($_SESSION['municipio']) {

                                    $respuesta = $select->consulta_lexico('20');
                                    $arreglo_respuesta_lexico = mysqli_fetch_array($respuesta);
                                    $complemento = $arreglo_respuesta_lexico['texto'];

                                    $cambio_palabra = count($arreglo_cadena) + 2;


                                    $Registrar = $inserts->mensaje_agente($arreglo_encuentra_resultado_municipio['nombre_completo'] . ' ' . $complemento . ' ');

                                    $_SESSION['numero_pregunta'] = 4;
                                } else {

                                    $_SESSION['numero_pregunta'] = 3;
                                }

                                $cambio_palabra = $cambio_palabra + 1;
                            }

                            $cambio_palabra = count($arreglo_cadena) + 2;

                            $busca_error = 0;
                        } elseif ($arreglo_encuentra_resultado['palabra'] == 'negacion') {


                            $activa_consulta = 1;
                            $consulta_municipios = $select->consulta_municipios($_SESSION['estado']);
                            $arreglo_consulta_municipio = mysqli_fetch_array($consulta_municipios);

                            $Registrar = $inserts->mensaje_agente($arreglo_respuesta['respuesta']);
                            $_SESSION['numero_pregunta'] = 22;

                            $busca_error = 0;
                            $cambio_palabra = count($arreglo_cadena) + 2;
                        }
                    } else {
                        $cambio_palabra = $cambio_palabra + 1;
                    }
                }
            } elseif ($_SESSION['numero_pregunta'] == 22) {

                $encuentra_resultado_municipio = $select->encuentra_resultado($arreglo_cadena[$cambio_palabra], 'municipios');
                $arreglo_encuentra_resultado_municipio = mysqli_fetch_array($encuentra_resultado_municipio);

                if ($arreglo_encuentra_resultado_municipio) {
                    $_SESSION['municipio'] = $arreglo_encuentra_resultado_municipio['nombre'];

                    //esta seccion posteriromente estar en un metodo para reulizar codigo 
                    $consulta_municipio = $select->consulta_municipio($_SESSION['municipio']);
                    $arreglo_consulta_municipio = mysqli_fetch_array($consulta_municipio);


                    $respuesta = $select->consulta_lexico('19');
                    $arreglo_respuesta_lexico = mysqli_fetch_array($respuesta);
                    $municipio = $arreglo_respuesta_lexico['texto'];


                    $mensaje_editado = ' ' . $municipio . '  ' . $arreglo_consulta_municipio['clima'];
                    $Registrar = $inserts->mensaje_agente($mensaje_editado);
                    //la numeracion cambia puesto que el num de pregunta hace referencia al siguiente
                    //cuestionamiento que se le ara al cliente
                    $_SESSION['numero_pregunta'] = 4;
                    $busca_error = 0;
                    $cambio_palabra = count($arreglo_cadena) + 2;
                } else {

                    $cambio_palabra = $cambio_palabra + 1;
                }
            } elseif ($_SESSION['numero_pregunta'] == 3) {

                $encuentra_resultado_municipio = $select->encuentra_resultado($arreglo_cadena[$cambio_palabra], 'municipios');
                $arreglo_encuentra_resultado_municipio = mysqli_fetch_array($encuentra_resultado_municipio);

                if ($arreglo_encuentra_resultado_municipio) {
                    $_SESSION['municipio'] = $arreglo_encuentra_resultado_municipio['nombre'];

                    //esta seccion posteriromente estar en un metodo para reulizar codigo 
                    $Registrar = $inserts->mensaje_agente($arreglo_respuesta['respuesta']);
                    $consulta_municipio = $select->consulta_municipio($_SESSION['municipio']);
                    $arreglo_consulta_municipio = mysqli_fetch_array($consulta_municipio);


                    $respuesta = $select->consulta_lexico('19');
                    $arreglo_respuesta_lexico = mysqli_fetch_array($respuesta);
                    $municipio = $arreglo_respuesta_lexico['texto'];


                    //$mensaje_editado = 'El municipio de ' + $_SESSION['municipio'] + "  Perteneciente al estado de   " + $_SESSION['estado']+ "cuenta con un clima " + $arreglo_consulta_municipio['clima'];
                    $mensaje_editado = ' ' . $municipio . ' ' . $arreglo_consulta_municipio['clima'];
                    $Registrar = $inserts->mensaje_agente($mensaje_editado);
                    //la numeracion cambia puesto que el num de pregunta hace referencia al siguiente
                    //cuestionamiento que se le ara al cliente
                    $_SESSION['numero_pregunta'] = 4;
                    $busca_error = 0;
                    $cambio_palabra = count($arreglo_cadena) + 2;
                } else {

                    $cambio_palabra = $cambio_palabra + 1;
                }
            } elseif ($_SESSION['numero_pregunta'] == 4) {
                //esta seccion es de sinonimos debe estar en una funcion o archivo para utilizarla varias ocaciones
                if ($arreglo_respuesta['tabla'] == 'sinonimos') {

                    //se manda palabra a palabra en la funcion para encontrar similitudes
                    $encuentra_resultado = $select->encuentra_resultado_sinonimo($arreglo_cadena[$cambio_palabra], $arreglo_respuesta['tabla']);
                    $arreglo_encuentra_resultado = mysqli_fetch_array($encuentra_resultado);

                    //se compara elarreglo si si tiene datos es que encontro un resultado
                    if ($arreglo_encuentra_resultado) {
                        //se valida con el tipo de pregunta que es lo que quiere el usuario y depende de la respuesta 
                        //se otorga una acción
                        if ($arreglo_encuentra_resultado['palabra'] == 'afirmacion') {

                            while ($cambio_palabra <= (count($arreglo_cadena) - 1)) {

                                $encuentra_resultado_sitio = $select->encuentra_sitios_turisticos($arreglo_cadena[$cambio_palabra], $_SESSION['municipio']);
                                $arreglo_encuentra_sito = mysqli_fetch_array($encuentra_resultado_sitio);


                                if ($arreglo_encuentra_sito) {
                                    $_SESSION['sitio'] = $arreglo_encuentra_sito['nombre'];
                                }
                                if ($_SESSION['sitio']) {

                                    $respuesta = $select->consulta_lexico('20');
                                    $arreglo_respuesta_lexico = mysqli_fetch_array($respuesta);
                                    $municipio = $arreglo_respuesta_lexico['texto'];



                                    $Registrar = $inserts->mensaje_agente($arreglo_encuentra_sito['nombre_completo'] . ' ' . $municipio . '');

                                    $_SESSION['numero_pregunta'] = 5;
                                } else {
                                      $_SESSION['numero_pregunta'] = 41;
                                }

                                $cambio_palabra = $cambio_palabra + 1;
                            }

                            $cambio_palabra = count($arreglo_cadena) + 2;

                            $busca_error = 0;
                        } elseif ($arreglo_encuentra_resultado['palabra'] == 'negacion') {
                            $_SESSION['numero_pregunta'] = 5;
                            $cambio_palabra = count($arreglo_cadena) + 2;
                            $busca_error = 0;
                        }
                    } else {
                        $cambio_palabra = $cambio_palabra + 1;
                    }
                }
            } elseif ($_SESSION['numero_pregunta'] == 41) {

                //esta seccion es de sinonimos debe estar en una funcion o archivo para utilizarla varias ocaciones
                if ($arreglo_respuesta['tabla'] == 'sinonimos') {
                    //se manda palabra a palabra en la funcion para encontrar similitudes
                    $encuentra_resultado = $select->encuentra_resultado_sinonimo($arreglo_cadena[$cambio_palabra], $arreglo_respuesta['tabla']);
                    $arreglo_encuentra_resultado = mysqli_fetch_array($encuentra_resultado);

                    //se compara elarreglo si si tiene datos es que encontro un resultado
                    if ($arreglo_encuentra_resultado) {
                        //se valida con el tipo de pregunta que es lo que quiere el usuario y depende de la respuesta 
                        //se otorga una accion
                        if ($arreglo_encuentra_resultado['palabra'] == 'afirmacion') {
                            while ($cambio_palabra <= (count($arreglo_cadena) - 1)) {

                                $encuentra_resultado_sitio = $select->encuentra_sitios_turisticos($arreglo_cadena[$cambio_palabra], $_SESSION['municipio']);
                                $arreglo_encuentra_sito = mysqli_fetch_array($encuentra_resultado_sitio);


                                if ($arreglo_encuentra_sito) {

                                    $_SESSION['sitio'] = $arreglo_encuentra_sito['nombre'];
                                    $cambio_palabra = count($arreglo_cadena) + 2;
                                }
                                if ($_SESSION['sitio']) {
                                    $Registrar = $inserts->mensaje_agente($arreglo_encuentra_sito['nombre_completo']);

                                    $_SESSION['numero_pregunta'] = 5;
                                } else {

                                    $_SESSION['numero_pregunta'] = 43;
                                }

                                $cambio_palabra = $cambio_palabra + 1;
                            }

                            $cambio_palabra = count($arreglo_cadena) + 2;

                            $busca_error = 0;
                        } elseif ($arreglo_encuentra_resultado['palabra'] == 'negacion') {

                            $activa_consulta = 1;
                            $encuentra_actividad = $select->consulta_actividades($_SESSION['municipio']);
                            $arreglo_encuentra_actividad = mysqli_fetch_array($encuentra_actividad);

                            //con esta seccion si si encontro los datos va a mostrar la informacion o la respuesta a dicha preguna 

                            $Registrar = $inserts->mensaje_agente($arreglo_respuesta['respuesta']);
                            $_SESSION['numero_pregunta'] = 42;

                            $busca_error = 0;
                            $cambio_palabra = count($arreglo_cadena) + 2;
                        }
                    } else {
                        $cambio_palabra = $cambio_palabra + 1;
                    }
                }
            } elseif ($_SESSION['numero_pregunta'] == 42) {

                $encuentra_resultado_sitio = $select->encuentra_sitios_turisticos($arreglo_cadena[$cambio_palabra], $_SESSION['municipio']);
                $arreglo_encuentra_sito = mysqli_fetch_array($encuentra_resultado_sitio);


                if ($arreglo_encuentra_sito) {
                    $_SESSION['numero_pregunta'] = 5;
                    $activa_consulta = 0;


                    $Registrar = $inserts->mensaje_agente($arreglo_respuesta['respuesta']);

                    $busca_error = 0;
                    $cambio_palabra = count($arreglo_cadena) + 2;

                    $_SESSION['sitio'] = $arreglo_encuentra_sito['nombre'];
                } else {
                    $cambio_palabra = $cambio_palabra + 1;
                }
            } elseif ($_SESSION['numero_pregunta'] == 43) {

                $encuentra_resultado_sitio = $select->encuentra_sitios_turisticos($arreglo_cadena[$cambio_palabra], $_SESSION['municipio']);
                $arreglo_encuentra_sito = mysqli_fetch_array($encuentra_resultado_sitio);


                if ($arreglo_encuentra_sito) {
                    $_SESSION['numero_pregunta'] = 5;
                    $activa_consulta = 0;

                    $respuesta = $select->consulta_lexico('21');
                    $arreglo_respuesta_lexico = mysqli_fetch_array($respuesta);
                    $direccion = $arreglo_respuesta_lexico['texto'];

                    $respuesta = $select->consulta_lexico('20');
                    $arreglo_respuesta_lexico = mysqli_fetch_array($respuesta);
                    $eleccion = $arreglo_respuesta_lexico['texto'];


                    $Registrar = $inserts->mensaje_agente($arreglo_encuentra_sito['nombre_completo'] . ' ' . $direccion . ' ' . $arreglo_encuentra_sito['ubicacion'] . '  ' . $eleccion . '');


                    $busca_error = 0;
                    $cambio_palabra = count($arreglo_cadena) + 2;

                    $_SESSION['sitio'] = $arreglo_encuentra_sito['nombre'];
                } else {
                    $cambio_palabra = $cambio_palabra + 1;
                }
            } elseif ($_SESSION['numero_pregunta'] == 5) {
                //esta sección es de sinónimos debe estar en una función o archivo para utilizarla varias ocasiones
                if ($arreglo_respuesta['tabla'] == 'sinonimos') {
                    //se manda palabra a palabra en la funcion para encontrar similitudes
                    $encuentra_resultado = $select->encuentra_resultado_sinonimo($arreglo_cadena[$cambio_palabra], $arreglo_respuesta['tabla']);
                    $arreglo_encuentra_resultado = mysqli_fetch_array($encuentra_resultado);

                    //se compara elarreglo si si tiene datos es que encontro un resultado
                    if ($arreglo_encuentra_resultado) {

                        //se valida con el tipo de pregunta que es lo que quiere el usuario y depende de la respuesta 
                        //se otorga una accion
                        if ($arreglo_encuentra_resultado['palabra'] == 'afirmacion') {
                            $activa_consulta = 1;

                            $encuentra_comida = $select->consulta_comida($_SESSION['municipio']);
                            $arreglo_encuentra_comida = mysqli_fetch_array($encuentra_comida);

                            //con esta seccion si si encontro los datos va a mostrar la informacion o la respuesta a dicha preguna 

                            $Registrar = $inserts->mensaje_agente($arreglo_respuesta['respuesta']);
                            $_SESSION['numero_pregunta'] = 6;

                            $busca_error = 0;
                            $cambio_palabra = count($arreglo_cadena) + 2;
                        } elseif ($arreglo_encuentra_resultado['palabra'] == 'negacion') {
                            //$Registrar = $inserts->mensaje_agente(' ');
                            $_SESSION['numero_pregunta'] = 6;
                            $cambio_palabra = count($arreglo_cadena) + 2;
                            $busca_error = 0;
                        }
                    } else {
                        $cambio_palabra = $cambio_palabra + 1;
                    }
                }
            } elseif ($_SESSION['numero_pregunta'] == 6) {
                //esta seccion es de sinonimos debe estar en una funcion o archivo para utilizarla varias ocaciones
                if ($arreglo_respuesta['tabla'] == 'sinonimos') {

                    //se manda palabra a palabra en la función para encontrar similitudes
                    $encuentra_resultado = $select->encuentra_resultado_sinonimo($arreglo_cadena[$cambio_palabra], 'sinonimos');
                    $arreglo_encuentra_resultado = mysqli_fetch_array($encuentra_resultado);

                    //se compara elarreglo si si tiene datos es que encontro un resultado
                    if ($arreglo_encuentra_resultado) {
                        //se valida con el tipo de pregunta que es lo que quiere el usuario y depende de la respuesta 
                        //se otorga una accion
                        if ($arreglo_encuentra_resultado['palabra'] == 'afirmacion') {


                            while ($cambio_palabra <= (count($arreglo_cadena) - 1)) {
                                $_SESSION['temporal'] = $arreglo_cadena[$cambio_palabra];
                                $encuentra_restaurante = $select->encuentra_restaurante($_SESSION['temporal'], $_SESSION['municipio']);
                                $arreglo_encuentra_restaurante = mysqli_fetch_array($encuentra_restaurante);


                                if ($arreglo_encuentra_restaurante) {
                                    $_SESSION['restaurante'] = $arreglo_encuentra_restaurante['nombre'];
                                }
                                if ($_SESSION['restaurante']) {

                                    $respuesta = $select->consulta_lexico('22');
                                    $arreglo_respuesta_lexico = mysqli_fetch_array($respuesta);
                                    $comida = $arreglo_respuesta_lexico['texto'];

                                    $Registrar = $inserts->mensaje_agente($arreglo_encuentra_restaurante['nombre_completo'] . ' ' . $comida . ' ');

                                    $_SESSION['numero_pregunta'] = 7;
                                } else {
                                    if($_SESSION['caso'] == 3)
                                    {
                                        $_SESSION['numero_pregunta'] = 62;
                                        
                                        $activa_consulta = 1;
                                        $encuentra_restaurante = $select->consulta_restaurantes($_SESSION['municipio']);
                                        $arreglo_encuentra_restaurante = mysqli_fetch_array($encuentra_restaurante);

                                    } else{
                                        
                                        $_SESSION['numero_pregunta'] = 61;    
                                    }
                                    
                                }

                                $cambio_palabra = $cambio_palabra + 1;
                            }

                            $cambio_palabra = count($arreglo_cadena) + 2;

                            $busca_error = 0;
                        } elseif ($arreglo_encuentra_resultado['palabra'] == 'negacion') {
                            //$Registrar = $inserts->mensaje_agente(' ');
                            $_SESSION['numero_pregunta'] = 7;
                            $cambio_palabra = count($arreglo_cadena) + 2;
                            $busca_error = 0;
                        }
                    } else {
                        $cambio_palabra = $cambio_palabra + 1;
                    }
                }
            } elseif ($_SESSION['numero_pregunta'] == 61) {

                //esta seccion es de sinonimos debe estar en una funcion o archivo para utilizarla varias ocaciones
                if ($arreglo_respuesta['tabla'] == 'sinonimos') {
                    //se manda palabra a palabra en la funcion para encontrar similitudes
                    $encuentra_resultado = $select->encuentra_resultado_sinonimo($arreglo_cadena[$cambio_palabra], $arreglo_respuesta['tabla']);
                    $arreglo_encuentra_resultado = mysqli_fetch_array($encuentra_resultado);

                    //se compara elarreglo si si tiene datos es que encontro un resultado
                    if ($arreglo_encuentra_resultado) {
                        //se valida con el tipo de pregunta que es lo que quiere el usuario y depende de la respuesta 
                        //se otorga una accion

                        if ($arreglo_encuentra_resultado['palabra'] == 'afirmacion') {

                            while ($cambio_palabra <= (count($arreglo_cadena) - 1)) {

                                $_SESSION['temporal'] = $arreglo_cadena[$cambio_palabra];
                                $encuentra_restaurante = $select->encuentra_restaurante($_SESSION['temporal'], $_SESSION['municipio']);
                               
                                $arreglo_encuentra_restaurante = mysqli_fetch_array($encuentra_restaurante);


                                if ($arreglo_encuentra_restaurante) {
                                    $_SESSION['restaurante'] = $arreglo_encuentra_restaurante['nombre'];
                                    $cambio_palabra = count($arreglo_cadena) + 2;
                                }
                                if ($_SESSION['restaurante']) {
                                    $respuesta = $select->consulta_lexico('22');
                                    $arreglo_respuesta_lexico = mysqli_fetch_array($respuesta);
                                    $comida = $arreglo_respuesta_lexico['texto'];

                                    $Registrar = $inserts->mensaje_agente($arreglo_encuentra_restaurante['nombre_completo'] . ' ' . $comida . '  ');

                                    $_SESSION['numero_pregunta'] = 7;
                                } else {

                                    $_SESSION['numero_pregunta'] = 63;
                                }

                                $cambio_palabra = $cambio_palabra + 1;
                            }

                            $cambio_palabra = count($arreglo_cadena) + 2;

                            $busca_error = 0;
                        } elseif ($arreglo_encuentra_resultado['palabra'] == 'negacion') {

                            $activa_consulta = 1;
                            $encuentra_restaurante = $select->consulta_restaurantes($_SESSION['municipio']);
                            $arreglo_encuentra_restaurante = mysqli_fetch_array($encuentra_restaurante);

                            //con esta seccion si si encontro los datos va a mostrar la informacion o la respuesta a dicha preguna 

                            $Registrar = $inserts->mensaje_agente($arreglo_respuesta['respuesta']);
                            $_SESSION['numero_pregunta'] = 62;

                            $busca_error = 0;
                            $cambio_palabra = count($arreglo_cadena) + 2;
                        }
                    } else {
                        $cambio_palabra = $cambio_palabra + 1;
                    }
                }
            } elseif ($_SESSION['numero_pregunta'] == 62) {


                $consulta_restaurante = $select->encuentra_restaurante($arreglo_cadena[$cambio_palabra], $_SESSION['municipio']);
                $arreglo_encuentra_restaurante = mysqli_fetch_array($consulta_restaurante);


                if ($arreglo_encuentra_restaurante) {
                    $_SESSION['numero_pregunta'] = 7;
                    $activa_consulta = 0;


                    $Registrar = $inserts->mensaje_agente($arreglo_respuesta['respuesta']);

                    $busca_error = 0;
                    $cambio_palabra = count($arreglo_cadena) + 2;

                    $_SESSION['restaurante'] = $arreglo_encuentra_restaurante['nombre'];
                } else {
                    $cambio_palabra = $cambio_palabra + 1;
                }
            } elseif ($_SESSION['numero_pregunta'] == 63) {


                $consulta_restaurante = $select->encuentra_restaurante($arreglo_cadena[$cambio_palabra], $_SESSION['municipio']);
                $arreglo_encuentra_restaurante = mysqli_fetch_array($consulta_restaurante);


                if ($arreglo_encuentra_restaurante) {
                    $_SESSION['numero_pregunta'] = 7;
                    $activa_consulta = 0;

                    $respuesta = $select->consulta_lexico('21');
                    $arreglo_respuesta_lexico = mysqli_fetch_array($respuesta);
                    $direccion = $arreglo_respuesta_lexico['texto'];

                    $Registrar = $inserts->mensaje_agente($arreglo_encuentra_restaurante['nombre_completo'] . ' ' . $direccion . '  ' . $arreglo_encuentra_restaurante['direccion']);

                    $busca_error = 0;
                    $cambio_palabra = count($arreglo_cadena) + 2;

                    $_SESSION['restaurante'] = $arreglo_encuentra_restaurante['nombre'];
                } else {
                    $cambio_palabra = $cambio_palabra + 1;
                }
            } elseif ($_SESSION['numero_pregunta'] == 7) {

                if ($arreglo_respuesta['tabla'] == 'sinonimos') {

                    //se manda palabra a palabra en la funcion para encontrar similitudes
                    $encuentra_resultado = $select->encuentra_resultado_sinonimo($arreglo_cadena[$cambio_palabra], $arreglo_respuesta['tabla']);
                    $arreglo_encuentra_resultado = mysqli_fetch_array($encuentra_resultado);


                    if ($arreglo_encuentra_resultado) {
                        //se valida con el tipo de pregunta que es lo que quiere el usuario y depende de la respuesta 
                        //se otorga una accion
                        if ($arreglo_encuentra_resultado['palabra'] == 'afirmacion') {

                            while ($cambio_palabra <= (count($arreglo_cadena) - 1)) {
                                //aquie me quede 
                                $encuentra_resultado_hotel = $select->encuentra_hotel($arreglo_cadena[$cambio_palabra], $_SESSION['municipio']);
                                $arreglo_encuentra_resultado_hotel = mysqli_fetch_array($encuentra_resultado_hotel);

                                if ($arreglo_encuentra_resultado_hotel) {
                                    $_SESSION['hotel'] = $arreglo_encuentra_resultado_hotel['nombre'];
                                }
                                if ($_SESSION['hotel']) {
                                    $Registrar = $inserts->mensaje_agente($arreglo_encuentra_resultado_hotel['nombre_completo'] . '  uno de los mejores hoteles ');
                                    $cambio_palabra = count($arreglo_cadena) + 2;
                                    $_SESSION['numero_pregunta'] = 8;
                                } else {
                                    
                                     if($_SESSION['caso'] == 3)
                                    {
                                     $activa_consulta = 1;
                                    $consulta_hoteles = $select->consulta_hoteles($_SESSION['municipio']);
                                    $arreglo_encuentra_actividad = mysqli_fetch_array($consulta_hoteles);

                                    $_SESSION['numero_pregunta'] = 72;
                                    }else
                                    {
                                        $_SESSION['numero_pregunta'] = 71;
                                    }
                                    
                                    }

                                $cambio_palabra = $cambio_palabra + 1;
                            }

                            $cambio_palabra = count($arreglo_cadena) + 2;

                            $busca_error = 0;
                        } elseif ($arreglo_encuentra_resultado['palabra'] == 'negacion') {

                            $_SESSION['numero_pregunta'] = 8;
                            $cambio_palabra = count($arreglo_cadena) + 2;
                            $busca_error = 0;
                        }
                    } else {
                        $cambio_palabra = $cambio_palabra + 1;
                    }
                }
            } elseif ($_SESSION['numero_pregunta'] == 71) {

                //esta seccion es de sinonimos debe estar en una funcion o archivo para utilizarla varias ocaciones
                if ($arreglo_respuesta['tabla'] == 'sinonimos') {
                    //se manda palabra a palabra en la funcion para encontrar similitudes
                    $encuentra_resultado = $select->encuentra_resultado_sinonimo($arreglo_cadena[$cambio_palabra], $arreglo_respuesta['tabla']);
                    $arreglo_encuentra_resultado = mysqli_fetch_array($encuentra_resultado);

                    //se compara el arreglo si si tiene datos es que encontro un resultado
                    if ($arreglo_encuentra_resultado) {
                        //se valida con el tipo de pregunta que es lo que quiere el usuario y depende de la respuesta 
                        //se otorga una acción
                        if ($arreglo_encuentra_resultado['palabra'] == 'afirmacion') {
                            while ($cambio_palabra <= (count($arreglo_cadena) - 1)) {

                                $encuentra_resultado_hotel = $select->encuentra_hotel($arreglo_cadena[$cambio_palabra], $_SESSION['municipio']);
                                $arreglo_encuentra_resultado_hotel = mysqli_fetch_array($encuentra_resultado_hotel);


                                if ($arreglo_encuentra_resultado_hotel) {
                                    $_SESSION['hotel'] = $arreglo_encuentra_resultado_hotel['nombre'];
                                    $cambio_palabra = count($arreglo_cadena) + 2;
                                }
                                if ($_SESSION['hotel']) {



                                    $respuesta = $select->consulta_lexico('23');
                                    $arreglo_respuesta_lexico = mysqli_fetch_array($respuesta);
                                    $hotel = $arreglo_respuesta_lexico['texto'];

                                    $Registrar = $inserts->mensaje_agente($arreglo_encuentra_sito['nombre_completo'] . ' ' . $hotel . '');

                                    $_SESSION['numero_pregunta'] = 8;
                                } else {

                                    $_SESSION['numero_pregunta'] = 73;
                                }

                                $cambio_palabra = $cambio_palabra + 1;
                            }

                            $cambio_palabra = count($arreglo_cadena) + 2;

                            $busca_error = 0;
                        } elseif ($arreglo_encuentra_resultado['palabra'] == 'negacion') {

                            $activa_consulta = 1;
                            $consulta_hoteles = $select->consulta_hoteles($_SESSION['municipio']);
                            $arreglo_encuentra_actividad = mysqli_fetch_array($consulta_hoteles);

                            //con esta seccion si si encontro los datos va a mostrar la informacion o la respuesta a dicha preguna 

                            $Registrar = $inserts->mensaje_agente($arreglo_respuesta['respuesta']);
                            $_SESSION['numero_pregunta'] = 72;

                            $busca_error = 0;
                            $cambio_palabra = count($arreglo_cadena) + 2;
                        }
                    } else {
                        $cambio_palabra = $cambio_palabra + 1;
                    }
                }
            } elseif ($_SESSION['numero_pregunta'] == 72) {

                $encuentra_encuentra_resultado_hotel = $select->encuentra_hotel($arreglo_cadena[$cambio_palabra], $_SESSION['municipio']);
                $arreglo_encuentra_resultado_hotel = mysqli_fetch_array($encuentra_encuentra_resultado_hotel);


                if ($arreglo_encuentra_resultado_hotel) {
                    $_SESSION['numero_pregunta'] = 8;
                    $activa_consulta = 0;


                    $respuesta = $select->consulta_lexico('23');
                    $arreglo_respuesta_lexico = mysqli_fetch_array($respuesta);
                    $hotel = $arreglo_respuesta_lexico['texto'];

                    $Registrar = $inserts->mensaje_agente($arreglo_encuentra_resultado_hotel['nombre_completo'] . ' ' . $hotel . '');

                    $busca_error = 0;
                    $cambio_palabra = count($arreglo_cadena) + 2;

                    $_SESSION['hotel'] = $arreglo_encuentra_resultado_hotel['nombre'];
                } else {
                    $cambio_palabra = $cambio_palabra + 1;
                }
            } elseif ($_SESSION['numero_pregunta'] == 73) {

                $encuentra_encuentra_resultado_hotel = $select->encuentra_hotel($arreglo_cadena[$cambio_palabra], $_SESSION['municipio']);
                $arreglo_encuentra_resultado_hotel = mysqli_fetch_array($encuentra_encuentra_resultado_hotel);


                if ($arreglo_encuentra_resultado_hotel) {
                    $_SESSION['numero_pregunta'] = 8;
                    $activa_consulta = 0;


                    $respuesta = $select->consulta_lexico('23');
                    $arreglo_respuesta_lexico = mysqli_fetch_array($respuesta);
                    $hotel = $arreglo_respuesta_lexico['texto'];

                    $Registrar = $inserts->mensaje_agente($arreglo_encuentra_resultado_hotel['nombre_completo'] . ' ' . $hotel . '');

                    $busca_error = 0;
                    $cambio_palabra = count($arreglo_cadena) + 2;

                    $_SESSION['hotel'] = $arreglo_encuentra_resultado_hotel['nombre'];
                } else {
                    $cambio_palabra = $cambio_palabra + 1;
                }
            } elseif ($_SESSION['numero_pregunta'] == 8) {
                //esta seccion es de sinonimos debe estar en una funcion o archivo para utilizarla varias ocaciones
                if ($arreglo_respuesta['tabla'] == 'sinonimos') {
                    //se manda palabra a palabra en la funcion para encontrar similitudes
                    $encuentra_resultado = $select->encuentra_resultado_sinonimo($arreglo_cadena[$cambio_palabra], $arreglo_respuesta['tabla']);
                    $arreglo_encuentra_resultado = mysqli_fetch_array($encuentra_resultado);

                    //se compara elarreglo si si tiene datos es que encontro un resultado
                    if ($arreglo_encuentra_resultado) {

                        //se valida con el tipo de pregunta que es lo que quiere el usuario y depende de la respuesta 
                        //se otorga una accion
                        if ($arreglo_encuentra_resultado['palabra'] == 'afirmacion') {
                            $activa_consulta = 1;
                            $encuentra_hoteles = $select->consulta_hotele($_SESSION['municipio']);
                            $arreglo_encuentra_hoteles = mysqli_fetch_array($encuentra_hoteles);

                            $Registrar = $inserts->mensaje_agente($arreglo_respuesta['respuesta']);
                            $_SESSION['numero_pregunta'] = 9;

                            $busca_error = 0;
                            $cambio_palabra = count($arreglo_cadena) + 2;
                        } elseif ($arreglo_encuentra_resultado['palabra'] == 'negacion') {
                            //$Registrar = $inserts->mensaje_agente(' ');
                            $_SESSION['numero_pregunta'] = 88;
                            $cambio_palabra = count($arreglo_cadena) + 2;
                            $busca_error = 0;
                        }
                    } else {
                        $cambio_palabra = $cambio_palabra + 1;
                    }
                }
            } elseif ($_SESSION['numero_pregunta'] == 88) {


                $encuentra_resultado_estado = $select->encuentra_estado($arreglo_cadena[$cambio_palabra]);
                $arreglo_encuentra_resultado_estado = mysqli_fetch_array($encuentra_resultado_estado);


                //se valida que se encontraran los datos y si si se asignan a las variables 
                if ($arreglo_encuentra_resultado_estado) {

                    $_SESSION['origen_estado'] = $arreglo_encuentra_resultado_estado['nombre'];

                    $cambio_palabra = count($arreglo_cadena) + 2;
                } else {
                    $cambio_palabra = $cambio_palabra + 1;
                }

                $_SESSION['origen'] = $cadena;

                if ($_SESSION['origen_estado']) {
                    $activa_consulta = 1;
                    $_SESSION['numero_pregunta'] = 11;


                    $busca_error = 0;
                } else {
                    $_SESSION['numero_pregunta'] = 88;
                    $activa_consulta = 1;
                }
                // $cambio_palabra = count($arreglo_cadena) + 2;
            } elseif ($_SESSION['numero_pregunta'] == 9) {


                $encuentra_resultado_estado = $select->encuentra_estado($arreglo_cadena[$cambio_palabra]);
                $arreglo_encuentra_resultado_estado = mysqli_fetch_array($encuentra_resultado_estado);


                //se valida que se encontraran los datos y si si se asignan a las variables 
                if ($arreglo_encuentra_resultado_estado) {

                    $_SESSION['origen_estado'] = $arreglo_encuentra_resultado_estado['nombre'];

                    $cambio_palabra = count($arreglo_cadena) + 2;
                } else {
                    $cambio_palabra = $cambio_palabra + 1;
                }

                $_SESSION['origen'] = $cadena;

                if ($_SESSION['origen_estado']) {
                    $activa_consulta = 1;
                    $_SESSION['numero_pregunta'] = 10;


                    $busca_error = 0;
                } else {
                    $_SESSION['numero_pregunta'] = 9;
                    $activa_consulta = 1;
                }
                // $cambio_palabra = count($arreglo_cadena) + 2;
            } elseif ($_SESSION['numero_pregunta'] == 10) {

                $activa_consulta = 1;
                $_SESSION['numero_pregunta'] = 11;

                $busca_error = 0;

                $cambio_palabra = count($arreglo_cadena) + 2;
            } elseif ($_SESSION['numero_pregunta'] == 11) {

                $encuentra_resultado = $select->encuentra_resultado_sinonimo($arreglo_cadena[$cambio_palabra], $arreglo_respuesta['tabla']);
                $arreglo_encuentra_resultado = mysqli_fetch_array($encuentra_resultado);
                //se compara elarreglo si si tiene datos es que encontro un resultado
                if ($arreglo_encuentra_resultado) {
                    //se valida con el tipo de pregunta que es lo que quiere el usuario y depende de la respuesta 
                    //se otorga una accion
                    if ($arreglo_encuentra_resultado['palabra'] == 'afirmacion') {

                        //con esta seccion si si encontro los datos va a mostrar la informacion o la respuesta a dicha preguna 
                        $Registrar = $inserts->mensaje_agente($arreglo_respuesta['respuesta']);
                        $_SESSION['numero_pregunta'] = 12;

                        $activa_consulta = 1;

                        $busca_error = 0;
                        $cambio_palabra = count($arreglo_cadena) + 2;

                        $consulta_paquetes = $select->consulta_paquete();

                        $arreglo_paquetes = mysqli_fetch_array($consulta_paquetes);
                    } elseif ($arreglo_encuentra_resultado['palabra'] == 'negacion') {
                        //se encontro respuesta
                        $cambio_palabra = count($arreglo_cadena) + 2;
                        //$Registrar = $inserts->mensaje_agente('ViajandoMx se despide, gracias por su visita ');
                        $_SESSION['numero_pregunta'] = 14;
                        $busca_error = 0;
                    }
                } else {

                    $cambio_palabra = $cambio_palabra + 1;
                }
            } elseif ($_SESSION['numero_pregunta'] == 12) {

                
                $encuentra_resultado = $select->encuentra_resultado_sinonimo($arreglo_cadena[$cambio_palabra],'sinonimos');
                $arreglo_encuentra_resultado = mysqli_fetch_array($encuentra_resultado);
            if ($arreglo_encuentra_resultado['palabra'] == 'negacion') {
                          
                        $cambio_palabra = count($arreglo_cadena) + 2;
                            $busca_error = 0;
                            if($_SESSION['hotel'])
                            {
                            $_SESSION['numero_pregunta'] = 14;
                            }
                            else
                            {
                               $_SESSION['numero_pregunta'] = 15;
                            }
                        }else{

                ///////////////////////////
                $encuentra_resultado_paquete = $select->encuentra_paquete($arreglo_cadena[$cambio_palabra]);
                $arreglo_encuentra_resultado_paquete = mysqli_fetch_array($encuentra_resultado_paquete);
                //se compara elarreglo si si tiene datos es que encontro un resultado
                if ($arreglo_encuentra_resultado_paquete) {
                    //se valida con el tipo de pregunta que es lo que quiere el usuario y depende de la respuesta 
                    //se otorga una accion
                    //con esta seccion si si encontro los datos va a mostrar la informacion o la respuesta a dicha preguna 
                    $Registrar = $inserts->mensaje_agente($arreglo_encuentra_resultado_paquete['nombre_completo'] . '  ' . $arreglo_respuesta['respuesta']);

                    $busca_error = 0;
                    $cambio_palabra = count($arreglo_cadena) + 2;
                    //se encontro respuesta
                    $_SESSION['paquete'] = $arreglo_encuentra_resultado_paquete['nombre'];
                    //se valida que el paquete seleccionado cuenta con vuelo y si no saltar a la siguiente pregunta
                    if ($_SESSION['paquete'] == 'Camion') {
                        $_SESSION['numero_pregunta'] = 14;
                    }
                    elseif($_SESSION['paquete'] == "basico")
                    {
                        $_SESSION['numero_pregunta'] = 14;
                        
                    }elseif($_SESSION['paquete'] == 'Vuelo/camion')
                    {
                         $_SESSION['numero_pregunta'] = 14;
                    }elseif($_SESSION['paquete'] == 'econoplus')
                    {
                        $_SESSION['numero_pregunta'] = 14;
                    }else {
                        $activa_consulta = 1;

                         
                        $consulta_aeropuertos = $select->consulta_aeropuertos($_SESSION['origen_estado']);

                        while ($arreglo_aeropuerto = mysqli_fetch_array($consulta_aeropuertos)) {

                            $Registrar = $inserts->mensaje_agente($arreglo_aeropuerto['nombre_completo'] . ' ubicada en la ciudad de ' . $arreglo_aeropuerto['ciudad']);
                        }

                        $_SESSION['numero_pregunta'] = 13;
                    }
                } else {

                    $cambio_palabra = $cambio_palabra + 1;
                }
            }
            } elseif ($_SESSION['numero_pregunta'] == 13) {

                $encuentra_resultado_paquete = $select->consulta_un_aeropuertos($arreglo_cadena[$cambio_palabra], $_SESSION['origen_estado']);
                $arreglo_encuentra_resultado_paquete = mysqli_fetch_array($encuentra_resultado_paquete);
                //se compara elarreglo si si tiene datos es que encontro un resultado
                if ($arreglo_encuentra_resultado_paquete) {
                    //se valida con el tipo de pregunta que es lo que quiere el usuario y depende de la respuesta 
                    //se otorga una accion
                    //con esta seccion si si encontro los datos va a mostrar la informacion o la respuesta a dicha preguna 
                    $Registrar = $inserts->mensaje_agente($_SESSION['aeropuerto'] . '  ' . $arreglo_respuesta['respuesta']);

                    $busca_error = 0;
                    $cambio_palabra = count($arreglo_cadena) + 2;
                    //se encontro respuesta

                    $_SESSION['numero_pregunta'] = 14;
                } else {

                    $cambio_palabra = $cambio_palabra + 1;
                }
            } elseif ($_SESSION['numero_pregunta'] == 14) {

                $encuentra_resultado = $select->encuentra_resultado_sinonimo($arreglo_cadena[$cambio_palabra], $arreglo_respuesta['tabla']);
                $arreglo_encuentra_resultado = mysqli_fetch_array($encuentra_resultado);
                //se compara elarreglo si si tiene datos es que encontro un resultado
                if ($arreglo_encuentra_resultado) {
                    //se valida con el tipo de pregunta que es lo que quiere el usuario y depende de la respuesta 
                    //se otorga una accion


                    if ($arreglo_encuentra_resultado['palabra'] == 'afirmacion') {

                        //con esta seccion si si encontro los datos va a mostrar la informacion o la respuesta a dicha preguna 
                        $Registrar = $inserts->mensaje_agente($arreglo_respuesta['respuesta']);
                        $_SESSION['numero_pregunta'] = 15;

                        if ($_SESSION['hotel']) {
                            $respuesta = $select->consulta_lexico('24');
                            $arreglo_respuesta_lexico = mysqli_fetch_array($respuesta);
                            $hotel = $arreglo_respuesta_lexico['texto'];

                            $respuesta = $select->consulta_lexico('25');
                            $arreglo_respuesta_lexico = mysqli_fetch_array($respuesta);
                            $ubicacion = $arreglo_respuesta_lexico['texto'];

                            $respuesta = $select->consulta_lexico('26');
                            $arreglo_respuesta_lexico = mysqli_fetch_array($respuesta);
                            $costo = $arreglo_respuesta_lexico['texto'];

                            $encuentra_datos_hotel = $select->datos_hotel($_SESSION['hotel'], $_SESSION['municipio']);
                            $arreglo_encuentra_datos_hotel = mysqli_fetch_array($encuentra_datos_hotel);
                            $Registrar = $inserts->mensaje_agente('' . $hotel . ' ' . $arreglo_encuentra_datos_hotel['nombre_completo'] . ' ' . $ubicacion . ' ' . $arreglo_encuentra_datos_hotel['ubicacion'] . ' ' . $costo . ' ' . $arreglo_encuentra_datos_hotel['precio']);
                            $total_hotel = $arreglo_encuentra_datos_hotel['precio'];
                        }
                        if ($_SESSION['restaurante']) {

                            $respuesta = $select->consulta_lexico('27');
                            $arreglo_respuesta_lexico = mysqli_fetch_array($respuesta);
                            $selecciona = $arreglo_respuesta_lexico['texto'];

                            $respuesta = $select->consulta_lexico('28');
                            $arreglo_respuesta_lexico = mysqli_fetch_array($respuesta);
                            $direccion = $arreglo_respuesta_lexico['texto'];

                            $encuentra_datos_restaurante = $select->datos_restaurante($_SESSION['restaurante'], $_SESSION['municipio']);
                            $arreglo_encuentra_datos_restaurante = mysqli_fetch_array($encuentra_datos_restaurante);

                            $Registrar = $inserts->mensaje_agente(' ' . $selecciona . ' ' . $arreglo_encuentra_datos_restaurante['nombre_completo'] . ' ' . $direccion . ' ' . $arreglo_encuentra_datos_restaurante['direccion']);
                        }
                        if ($_SESSION['paquete']) {

                            $respuesta = $select->consulta_lexico('29');
                            $arreglo_respuesta_lexico = mysqli_fetch_array($respuesta);
                            $paquete = $arreglo_respuesta_lexico['texto'];

                            $respuesta = $select->consulta_lexico('30');
                            $arreglo_respuesta_lexico = mysqli_fetch_array($respuesta);
                            $costo = $arreglo_respuesta_lexico['texto'];

                            $encuentra_datos_paquete = $select->datos_paquete($_SESSION['paquete']);
                            $arreglo_encuentra_datos_paquete = mysqli_fetch_array($encuentra_datos_paquete);
                            $Registrar = $inserts->mensaje_agente('' . $paquete . ' ' . $arreglo_encuentra_datos_paquete['nombre_completo'] . ' ' . $costo . ' ' . $arreglo_encuentra_datos_paquete['costo']);

                            $total_paquete = $arreglo_encuentra_datos_paquete['costo'];
                        }
                        if ($_SESSION['aeropuerto']) {

                            $respuesta = $select->consulta_lexico('31');
                            $arreglo_respuesta_lexico = mysqli_fetch_array($respuesta);
                            $aeropuerto = $arreglo_respuesta_lexico['texto'];


                            $encuentra_datos_trasporte = $select->datos_trasporte($_SESSION['estado'], $_SESSION['aeropuerto']);
                            $arreglo_encuentra_datos_trasporte = mysqli_fetch_array($encuentra_datos_trasporte);
                            $Registrar = $inserts->mensaje_agente('' . $aeropuerto . ' ' . $arreglo_encuentra_datos_trasporte['nombre_completo']);
                        }

                        $activa_consulta = 1;

                        $busca_error = 0;
                        $cambio_palabra = count($arreglo_cadena) + 2;

                        $consulta_paquetes = $select->consulta_paquete();

                        $arreglo_paquetes = mysqli_fetch_array($consulta_paquetes);
                    } elseif ($arreglo_encuentra_resultado['palabra'] == 'negacion') {
                        //se encontro respuesta
                        $cambio_palabra = count($arreglo_cadena) + 2;
                        //$Registrar = $inserts->mensaje_agente('ViajandoMx se despide, gracias por su visita ');
                        $_SESSION['numero_pregunta'] = 15;
                        $busca_error = 0;
                    }
                } else {

                    $cambio_palabra = $cambio_palabra + 1;
                }
            } elseif ($_SESSION['numero_pregunta'] == 15) {
                //esta seccion es de sinonimos debe estar en una funcion o archivo para utilizarla varias ocaciones
                if ($arreglo_respuesta['tabla'] == 'sinonimos') {
                    //se manda palabra a palabra en la funcion para encontrar similitudes
                    $encuentra_resultado = $select->encuentra_resultado_sinonimo($arreglo_cadena[$cambio_palabra], $arreglo_respuesta['tabla']);
                    $arreglo_encuentra_resultado = mysqli_fetch_array($encuentra_resultado);

                    //se compara elarreglo si si tiene datos es que encontro un resultado
                    if ($arreglo_encuentra_resultado) {

                        if ($arreglo_encuentra_resultado['palabra'] == 'afirmacion') {
                            $activa_consulta = 1;

                            $Registrar = $inserts->mensaje_agente($arreglo_respuesta['respuesta']);
                            $_SESSION['numero_pregunta'] = 2;

                            $busca_error = 0;
                            $cambio_palabra = count($arreglo_cadena) + 2;



                            $_SESSION['estado'] = "";
                            $_SESSION['municipio'] = "";
                            $_SESSION['clima'] = "";
                            $_SESSION['hotel'] = "";
                            $_SESSION['restaurante'] = "";
                            $_SESSION['comida'] = "";

                            $_SESSION['origen'] = "";
                        } elseif ($arreglo_encuentra_resultado['palabra'] == 'negacion') {

                            $respuesta = $select->consulta_lexico('32');
                            $arreglo_respuesta_lexico = mysqli_fetch_array($respuesta);
                            $despedida = $arreglo_respuesta_lexico['texto'];


                            $Registrar = $inserts->mensaje_agente($despedida);
                            //$_SESSION['numero_pregunta']=8;
                            $cambio_palabra = count($arreglo_cadena) + 2;
                            $busca_error = 0;

                            $_SESSION['numero_pregunta'] = 16;
                        }
                    } else {
                        $cambio_palabra = $cambio_palabra + 1;
                    }
                }
            } elseif ($_SESSION['numero_pregunta'] == 16) {
                $_SESSION['numero_pregunta'] = 0;

                header("Location:http://localhost/ViajAndo/index.php");
                $cambio_palabra = count($arreglo_cadena) + 2;
            } elseif ($_SESSION['numero_pregunta'] == 101) {

                $encuentra_resultado = $select->encuentra_resultado_sinonimo($arreglo_cadena[$cambio_palabra], $arreglo_respuesta['tabla']);
                $arreglo_encuentra_resultado = mysqli_fetch_array($encuentra_resultado);
                //se compara elarreglo si si tiene datos es que encontro un resultado
                if ($arreglo_encuentra_resultado) {
                    //se valida con el tipo de pregunta que es lo que quiere el usuario y depende de la respuesta 
                    //se otorga una accion
                    if ($arreglo_encuentra_resultado['palabra'] == 'afirmacion') {



                        //con esta seccion si si encontro los datos va a mostrar la informacion o la respuesta a dicha preguna 
                        $Registrar = $inserts->mensaje_agente($arreglo_respuesta['respuesta']);


                        $_SESSION['numero_pregunta'] = 102;
                        $busca_error = 0;
                        $cambio_palabra = count($arreglo_cadena) + 2;
                        $activa_consulta = 1;
                    } elseif ($arreglo_encuentra_resultado['palabra'] == 'negacion') {
                        //se encontro respuesta

                        $respuesta = $select->consulta_lexico('32');
                        $arreglo_respuesta_lexico = mysqli_fetch_array($respuesta);
                        $despedida = $arreglo_respuesta_lexico['texto'];



                        $Registrar = $inserts->mensaje_agente($despedida);
                        //$_SESSION['numero_pregunta']=8;
                        $cambio_palabra = count($arreglo_cadena) + 2;
                        $busca_error = 0;

                        $_SESSION['numero_pregunta'] = 16;
                    }
                } else {

                    $cambio_palabra = $cambio_palabra + 1;
                }
            } elseif ($_SESSION['numero_pregunta'] == 102) {

                $encuentra_actividad = $select->busca_actividad($arreglo_cadena[$cambio_palabra]);
                $arreglo_encuentra_actividad = mysqli_fetch_array($encuentra_actividad);
                //se compara elarreglo si si tiene datos es que encontro un resultado
                if ($arreglo_encuentra_actividad) {

                    // $Registrar = $inserts->mensaje_agente($arreglo_respuesta['respuesta']);
                    $_SESSION['numero_pregunta'] = 103;
                    $busca_error = 0;
                    $activa_consulta = 1;
                    $cambio_palabra = count($arreglo_cadena) + 2;
                    $_SESSION['actividad'] = $arreglo_encuentra_actividad['nombre'];
                } else {

                    $cambio_palabra = $cambio_palabra + 1;
                }
            } elseif ($_SESSION['numero_pregunta'] == 103) {


                $climas_actividades = $select->busca_clima_relacion($arreglo_cadena[$cambio_palabra], $_SESSION['actividad']);
                $arreglo_encuentra_clima = mysqli_fetch_array($climas_actividades);
                //se compara elarreglo si si tiene datos es que encontro un resultado
                if ($arreglo_encuentra_clima) {

                    // $Registrar = $inserts->mensaje_agente($arreglo_respuesta['respuesta']);

                    $busca_error = 0;
                    $cambio_palabra = count($arreglo_cadena) + 2;

                    $_SESSION['clima'] = $arreglo_encuentra_clima['nombre_corto'];
                    // se hace esta validacion si se selecciona un clima y actividad que hagan que no tenga hoteles con esas caracteristicas 
                    $hotel_categoria = $select->consulta_hoteles_relacion($_SESSION['actividad'], $_SESSION['clima']);
                    if ($arreglo_hotel_categoria = mysqli_fetch_array($hotel_categoria)) {
                        $_SESSION['numero_pregunta'] = 104;
                        $activa_consulta = 1;
                    } else {
                        $_SESSION['numero_pregunta'] = 105;
                        $activa_consulta = 1;
                    }
                } else {

                    $cambio_palabra = $cambio_palabra + 1;
                }
            } elseif ($_SESSION['numero_pregunta'] == 104) {
                $clasidicacion_hotel = $select->busca_categoria_hoteles($_SESSION['clima'], $_SESSION['actividad'], ($arreglo_cadena[$cambio_palabra]));
                $arreglo_clasidicacion_hotel = mysqli_fetch_array($clasidicacion_hotel);
                //se compara elarreglo si si tiene datos es que encontro un resultado
                if ($arreglo_clasidicacion_hotel) {

                    // $Registrar = $inserts->mensaje_agente($arreglo_respuesta['respuesta']);
                    $_SESSION['numero_pregunta'] = 105;
                    $busca_error = 0;
                    $cambio_palabra = count($arreglo_cadena) + 2;
                    $activa_consulta = 2;
                    $_SESSION['clasificacion_hotel'] = $arreglo_clasidicacion_hotel['nombre_corto'];
                } else {

                    $cambio_palabra = $cambio_palabra + 1;
                }
            } elseif ($_SESSION['numero_pregunta'] == 105) {

                $encuentra_resultado = $select->encuentra_resultado_sinonimo($arreglo_cadena[$cambio_palabra], 'sinonimos');
                $arreglo_encuentra_resultado = mysqli_fetch_array($encuentra_resultado);

                if ($arreglo_encuentra_resultado['palabra'] == 'negacion') {
                    //se encontro respuesta



                    $respuesta = $select->consulta_lexico('32');
                    $arreglo_respuesta_lexico = mysqli_fetch_array($respuesta);
                    $despedida = $arreglo_respuesta_lexico['texto'];

                    $Registrar = $inserts->mensaje_agente($despedida);
                    $_SESSION['numero_pregunta'] = 15;
                    $busca_error = 0;
                    $cambio_palabra = count($arreglo_cadena) + 2;
                } else {
                    if ($_SESSION['clasificacion_hotel']) {

                        $climas_actividades = $select->consultas_sitio_relacion_hotel($_SESSION['clima'], $_SESSION['actividad'], $_SESSION['clasificacion_hotel'], $arreglo_cadena[$cambio_palabra]);
                        $arreglo_encuentra_clima = mysqli_fetch_array($climas_actividades);
                        //se compara elarreglo si si tiene datos es que encontro un resultado
                        if ($arreglo_encuentra_clima) {

                            // $Registrar = $inserts->mensaje_agente($arreglo_respuesta['respuesta']);
                            $_SESSION['numero_pregunta'] = 6;
                            $busca_error = 0;

                             $_SESSION['estado'] = $arreglo_encuentra_clima['estado'];
                             $_SESSION['municipio'] = $arreglo_encuentra_clima['municipio'];
                            $cambio_palabra = count($arreglo_cadena) + 2;

                            //$_SESSION['clima'] =$arreglo_encuentra_actividad['nombre_corto'];
                        } else {

                            $cambio_palabra = $cambio_palabra + 1;
                        }
                    } else {
                        $climas_actividades = $select->consulta_sitio_relacion_normal($_SESSION['clima'], $_SESSION['actividad'], $arreglo_cadena[$cambio_palabra]);
                        $arreglo_encuentra_clima = mysqli_fetch_array($climas_actividades);
                        //se compara elarreglo si si tiene datos es que encontro un resultado
                        if ($arreglo_encuentra_clima) {

                            // $Registrar = $inserts->mensaje_agente($arreglo_respuesta['respuesta']);
                            $_SESSION['numero_pregunta'] = 6;
                            $busca_error = 0;
                            $_SESSION['estado'] = $arreglo_encuentra_clima['estado'];
                            $_SESSION['municipio'] = $arreglo_encuentra_clima['municipio'];
                            $cambio_palabra = count($arreglo_cadena) + 2;

                            // $_SESSION['clima'] =$arreglo_encuentra_actividad['nombre_corto'];
                        } else {

                            $cambio_palabra = $cambio_palabra + 1;
                        }
                    }
                }
            }
        }

        if ($busca_error == 1) {
            
            
            $Registrar = $inserts->mensaje_agente($arreglo_respuesta['respuesta_erronea']);
            //$Registrar = $inserts->mensaje_agente("No logre entenderle comencemos de nuevo, disculpe las molestias  ");
        }
    }



//metodo para borrar la conversacion actual
    if (isset($_POST['borrar'])) {

        $Registrar = $borrar->borrar_chat();
    }



    //para tener el control de la listas de preguntas y respuestas 



    $Pregunta = $select->preguntas_agente($_SESSION['numero_pregunta']);
    $pregunta_arreglo = mysqli_fetch_array($Pregunta);
//se coloca 0 al numero de pregunta y como no existe no se hace la insercion esto es
//para cuando se ingresan


    if ($_SESSION['numero_pregunta'] != 0) {
        $Registrar = $inserts->mensaje_agente($pregunta_arreglo['pregunta']);
    }

    $obtner_conversacion = $select->ver_conversacion($_SESSION['id_usuario']);
    ?>