<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Funciones {

    /**
     * Función para quitar acentos a las palabras ingresadas por el usuario
     * @param string $cadena
     * @return String
     */
    function Quitar_acentos($cadena) {
        return utf8_encode(strtr($cadena, utf8_decode('àáâãäçèéêëìíîïóòôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝ'), 'aaaaaceeeeiiiiooooouuuuyyAAAAACEEEEIIIIOOOOOUUUUY'));
    }

}
