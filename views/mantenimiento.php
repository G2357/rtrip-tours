<?php
/* * *****************************************************************************
 * Objetivo: Página placeholder de enlaces                                     *
 * Autor: Noel Juan Chavero                            Fecha: 05/03/2017       *
 * ***************************************************************************** */
?>
<head>
    <title>Trabajo en Progreso</title>
    <link href="http://127.0.0.1/RTripTours/CSS/Estilos/cover.css" rel="stylesheet">
</head>
<body>
    <div class="site-wrapper" style="background-image: url('http://127.0.0.1/RTripTours/images/pexels-photo-30732.jpg')">
        <table width="100%" height="100%">
            <tr>
                <td valign="middle" align="center">
                    <img src="/RTripTours/images/mantenimiento.png" alt="En mantenimiento disculpe las molestias...">
                    <br>
                    <h1><a href="/RTripTours/index.php">Regresar a la ventana principal</a></h1>
                </td>
            </tr>
        </table>
    </div>
</body>