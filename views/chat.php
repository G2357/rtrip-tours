<!--
Nombre del archivo: index.php
Autor original: Scrum team
Fecha de creacion de archivo:27 de marzo de 2016
Descripcion: Encuentra el menu de chateo y las comparaciones necesarias para mostras las respuestas adecuadas al usuario
-->

<!DOCTYPE html>
<html>
    <br>


    <?php
    //require_once("../models/chat_model.php");
    include("../_shared/head.php");
    ?>

    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>

    <!-- son las separaciones para colocar el nombre del usuario-->

    <br>
    <br>
    <!-- con esta seccion se esta creando la estructura de la tabla que contendra el chat-->


    <div class="col-md-4 ">

        <form method="POST" action="" name="limpiar" id="chat" >


        </form>
        <!--           Inicia tabla que contiene la conversación-->
        <table   class="table bg-info" id='Tabla_conversacion'>
            <thead> 
            <td colspan="3" class="text-center">RTrip Tours</td>

            <a  class="btn btn-danger" href="http://localhost/RTripTours/index.php">Salir</a>
            <tr  class="bg-primary">
                <td></td>
                <th colspan="3"   >Mensajes</th>
                <th></th>
            </tr>
            </thead>
            <tbody> 
<?php while ($row = mysqli_fetch_array($obtner_conversacion)) { ?>

                    <tr>
                        <td>

                            <?php
                            if ($row['tipo'] == 'cliente') {
                                ?>  
                                <!-- Etiqueta para colocar la imagen del cliente-->
                                <img src=<?php echo $row['imagen']; ?> width="80" height="80" > 
                                <?php
                                echo $row['nombre'];
                            }
                            ?>
                        </td>
                        <td colspan="3" >

                            <div class="panel  <?php echo $row['color']; ?>"   >
    <?php echo $row['mensaje'] ?>
                            </div>

                        </td>
                        <td>
                            <?php
                            if ($row['tipo'] == 'agente') {
                                ?>                          
                                <!--   Etiqueta para colocar la imagen del agente inteligente -->
                                <img src=<?php echo $row['imagen']; ?> width="80" height="80" > 
                                <?php
                                echo $row['nombre'];
                            }
                            ?>
                        </td>
                    </tr>
<?php } ?>

                <tr>
                    <td colspan="5">
                        <form method="POST" action="" name="chat" id="chat" >
                            <!--  cuadro de texto para comunicación con el sistema -->
                            <input type="text" name="usuario[mensaje]" id="usuario[mensaje] cuadro_conversacion" class="form-control">

                            <?php if ($_SESSION['numero_pregunta'] == 10) {
                                ?>                          <!--           Boton de consulta mapa -->
                                <a class="btn btn-info form-control" href="javascript:void(0)" id="search" class="send" >Buscar Ruta</a>

                                <?php
                            }
                            ?>
                            <!--  Boton de enviar información-->
                            <input class="btn btn-info  form-control" id='boton_contestar' type="submit" value="Enviar"> 
                        </form>

                    </td>
                </tr>
            </tbody>
        </table>
        <br>
        <br>
        <br>
        <br>
        <br>


    </div>
    <br>
    <br>
    <div class="col-md-8">

        <?php
        if ($_SESSION['numero_pregunta'] == 105 && $activa_consulta == 2) {
            ?>
            <table  class="table  table-bordered bg-info " id='tabla_paquetes'>
                <thead>
                    <tr >
                        <th >Nombre sitio</th>
                        <th >Direccion</th>
                        <th >Municipio</th>
                        <th >Estado</th>

                    </tr>
                </thead>
                <tbody> 
                    <?php
                    // problemas por que solo da dos datos y la consulta esta bien

                    $hotel_categoria = $conversacion->busca_sitio_relacion_normal($_SESSION['clima'], $_SESSION['actividad']);
                    while ($arreglo_hotel_categoria = mysqli_fetch_array($hotel_categoria)) {
                        ?>

                        <tr>
                            <td>
        <?php echo $arreglo_hotel_categoria['nombre_completo'] ?>
                            </td>
                            <td>
        <?php echo $arreglo_hotel_categoria['ubicacion'] ?>
                            </td>
                            <td>
        <?php echo $arreglo_hotel_categoria['municipio'] ?>
                            </td>
                            <td>
        <?php echo $arreglo_hotel_categoria['estado'] ?>
                            </td>
                        </tr>
                    <?php }
                    ?>

                </tbody>
            </table>
<?php } ?>



        <?php
        if ($_SESSION['numero_pregunta'] == 105 && $activa_consulta == 1) {
            ?>
            <table  class="table  table-bordered bg-info " id='tabla_paquetes'>
                <thead>
                    <tr  >
                        <th >Nombre sitio</th>
                        <th >Direccion</th>
                        <th >Municipio</th>
                        <th >Estado</th>

                    </tr>
                </thead>
                <tbody> 
                    <?php
                    // problemas por que solo da dos datos y la consulta esta bien

                    $hotel_categoria = $conversacion->busca_sitio_relacion_hotel($_SESSION['clima'], $_SESSION['actividad'], $_SESSION['casifiacion_hotel']);
                    while ($arreglo_hotel_categoria = mysqli_fetch_array($hotel_categoria)) {
                        ?>

                        <tr>
                            <td>
        <?php echo $arreglo_hotel_categoria['nombre_completo'] ?>
                            </td>
                            <td>
        <?php echo $arreglo_hotel_categoria['ubicacion'] ?>
                            </td>
                            <td>
        <?php echo $arreglo_hotel_categoria['municipio'] ?>
                            </td>
                            <td>
        <?php echo $arreglo_hotel_categoria['estado'] ?>
                            </td>
                        </tr>
                    <?php }
                    ?>

                </tbody>
            </table>
<?php } ?>



        <?php
        if ($_SESSION['numero_pregunta'] == 104 && $activa_consulta == 1) {
            ?>
            <table  class="table  table-bordered bg-info " id='tabla_paquetes'>
                <thead>
                    <tr  >
                        <th ></th>

                    </tr>
                </thead>
                <tbody> 
                    <?php
                    // problemas por que solo da dos datos y la consulta esta bien

                    $hotel_categoria = $select->consulta_hoteles_relacion($_SESSION['actividad'], $_SESSION['clima']);
                    while ($arreglo_hotel_categoria = mysqli_fetch_array($hotel_categoria)) {
                        ?>

                        <tr>
                            <td>
        <?php echo $arreglo_hotel_categoria['categoria'] ?>
                            </td>

                        </tr>
                    <?php }
                    ?>

                </tbody>
            </table>
<?php } ?>


        <?php
        if ($_SESSION['numero_pregunta'] == 103 && $activa_consulta == 1) {
            ?>
            <table  class="table  table-bordered bg-info " id='tabla_paquetes'>
                <thead>
                    <tr >
                        <th ></th>

                    </tr>
                </thead>
                <tbody> 
                    <?php
                    // problemas por que solo da dos datos y la consulta esta bien

                    $climas_actividades = $select->consulta_clima_relacion($_SESSION['actividad']);
                    while ($arreglo_climas_actividades = mysqli_fetch_array($climas_actividades)) {
                        ?>

                        <tr>
                            <td class="center-block" >
        <?php echo $arreglo_climas_actividades['clima'] ?>
                            </td>

                        </tr>
                    <?php }
                    ?>

                </tbody>
            </table>
        <?php } ?>

        <?php
        $actividades = $select->consulta_actividades_tabla();

        if ($_SESSION['numero_pregunta'] == 102 && $activa_consulta == 1) {
            ?>
            <table  class="table  table-bordered bg-info " id='tabla_paquetes'>
                <thead>
                    <tr >
                        <th ></th>

                    </tr>
                </thead>
                <tbody> 
                    <?php
                    // problemas por que solo da dos datos y la consulta esta bien


                    while ($arreglo_actividades = mysqli_fetch_array($actividades)) {
                        ?>

                        <tr>
                            <td class="center-block" >
        <?php echo $arreglo_actividades['nombre_completo'] ?>
                            </td>

                        </tr>
                    <?php }
                    ?>

                </tbody>
            </table>
        <?php } ?>

        <?php
        if ($_SESSION['numero_pregunta'] == 12 && $activa_consulta == 1) {
            ?>
            <table  class="table  table-bordered bg-info " id='tabla_paquetes'>
                <thead>
                    <tr >
                        <th colspan="3" ></th>
                        <th colspan="3"></th>
                        <th colspan="3"></th>
                    </tr>
                </thead>
                <tbody> 
                    <?php
                    // problemas por que solo da dos datos y la consulta esta bien
                    $consulta_paquetes = $select->consulta_paquete();


                    while ($arreglo_paquetes = mysqli_fetch_array($consulta_paquetes)) {
                        ?>

                        <tr>
                            <td >
        <?php echo $arreglo_paquetes['nombre_completo'] ?>
                            </td>
                            <td>
        <?php echo $arreglo_paquetes['descripcion'] ?>
                            </td>

                            <td colspan="3">
        <?php echo '$' . $arreglo_paquetes['costo'] ?>
                            </td>
                        </tr>
                    <?php }
                    ?>

                </tbody>
            </table>
<?php } ?>


        <?php
        if ($_SESSION['numero_pregunta'] == 72 && $activa_consulta == 1) {
            ?>
            <table  class="table  table-bordered bg-info " id='tabla_hoteles'>
                <thead>
                    <tr  >
                        <th colspan="3" ></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody> 
                    <?php
                    // problemas por que solo da dos datos y la consulta esta bien
                    $consulta_hoteles = $select->consulta_hoteles($_SESSION['municipio']);

                    while ($arreglo_encuentra_hoteles = mysqli_fetch_array($consulta_hoteles)) {
                        ?>

                        <tr>
                            <td colspan="3">
        <?php echo $arreglo_encuentra_hoteles['nombre_completo'] ?>
                            </td>
                            <td>
        <?php echo $arreglo_encuentra_hoteles['ubicacion'] ?>
                            </td>

                            <td>
        <?php echo ' $ ' . $arreglo_encuentra_hoteles['precio'] ?>
                            </td>
                        </tr>
                    <?php }
                    ?>

                </tbody>
            </table>
<?php } ?>


        <?php
        if ($_SESSION['numero_pregunta'] == 62 && $activa_consulta == 1) {
            ?>
            <table  class="table  table-bordered bg-info " id='tabla_restaurantes'>
                <thead>
                    <tr >
                        <th colspan="3" ></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody> 
                    <?php
                    $encuentra_restaurante = $select->consulta_restaurantes($_SESSION['municipio']);

                    while ($arreglo_encuentra_restaurante = mysqli_fetch_array($encuentra_restaurante)) {
                        ?>

                        <tr>
                            <td colspan="3">
        <?php echo $arreglo_encuentra_restaurante['nombre_completo'] ?>
                            </td>
                            <td>
        <?php echo $arreglo_encuentra_restaurante['direccion'] ?>
                            </td>
                        </tr>
                    <?php }
                    ?>

                </tbody>
            </table>
        <?php } ?>

        <?php
        if ($_SESSION['numero_pregunta'] == 42 && $activa_consulta == 1) {
            ?>
            <table  class="table  table-bordered bg-info " id='tabla_actividad'>
                <thead>
                    <tr  >
                        <th colspan="3" ></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody> 
                    <?php
                    $encuentra_actividad = $select->consulta_actividades($_SESSION['municipio']);

                    while ($arreglo_encuentra_actividad = mysqli_fetch_array($encuentra_actividad)) {
                        ?>

                        <tr>
                            <td colspan="3">
                                <?php echo $arreglo_encuentra_actividad['nombre_completo'] ?>
                            </td>
                            <td>
                        <?php echo $arreglo_encuentra_actividad['ubicacion'] ?>
                            </td>
                        </tr>
    <?php }
    ?>

                </tbody>
            </table>
        <?php } ?>


        <?php
        if ($_SESSION['numero_pregunta'] == 22 && $activa_consulta == 1) {
            ?>
            <table  class="table  table-bordered bg-info " id='tabla_municipio'>
                <thead>
                    <tr  >
                        <th colspan="3" ></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody> 
    <?php while ($arreglo_consulta_municipios = mysqli_fetch_array($consulta_municipios)) { ?>

                        <tr>
                            <td colspan="3">
                                <?php echo $arreglo_consulta_municipios['id_municipio'] ?>
                            </td>
                            <td>
                        <?php echo $arreglo_consulta_municipios ['nombre_completo'] ?>
                            </td>
                        </tr>
    <?php }
    ?>

                </tbody>
            </table>
        <?php } ?>

        <?php
        if ($_SESSION['numero_pregunta'] == 5 && $activa_consulta == 1) {
            ?>
            <table  class="table  table-bordered bg-info" id='tabla_actividad'>
                <thead>
                    <tr  >
                        <th colspan="3" ></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody> 
    <?php while ($arreglo_encuentra_actividad = mysqli_fetch_array($encuentra_actividad)) { ?>

                        <tr>
                            <td colspan="3">
                                <?php echo $arreglo_encuentra_actividad['nombre'] ?>
                            </td>
                            <td>
                        <?php echo $arreglo_encuentra_actividad ['ubicacion'] ?>
                            </td>
                        </tr>
    <?php }
    ?>

                </tbody>
            </table>
        <?php } ?>

<?php if ($_SESSION['numero_pregunta'] == 6 && $activa_consulta == 1) {
    ?>
            <table  class="table  table-bordered bg-info" id='tabla_comida'>
                <thead>
                    <tr  >
                        <th></th>
                    </tr>
                </thead>
                <tbody> 
                    <?php
                    $encuentra_comida = $select->consulta_comida($_SESSION['municipio']);

                    while ($arreglo_encuentra_comida = mysqli_fetch_array($encuentra_comida)) {
                        ?>

                        <tr>
                            <td class="text-center" >
                        <?php echo $arreglo_encuentra_comida['nombre_comida'] ?>
                            </td>

                        </tr>
    <?php } ?>

                </tbody>

            </table>
        <?php } ?>

<?php
if ($_SESSION['numero_pregunta'] == 7 && $activa_consulta == 1) {
    ?>
            <table  class="table  table-bordered bg-info" id='tabla_restaurantes'>
                <thead>
                    <tr  >
                        <th colspan="4" ></th>

                    </tr>
                </thead>
                <tbody> 
                            <?php while ($arreglo_encuentra_restaurantes = mysqli_fetch_array($encuentra_restaurantes)) { ?>

                        <tr>
                            <td colspan="3">
                                <?php echo $arreglo_encuentra_restaurantes['nombre'] ?>
                            </td>
                            <td>
                        <?php echo $arreglo_encuentra_restaurantes ['direccion'] ?>
                            </td>
                        </tr>
    <?php }
    ?>

                </tbody>
            </table>
            <?php
        }
        if ($_SESSION['numero_pregunta'] == 8 && $activa_consulta == 1) {
            ?>

            <table  class="table  table-bordered bg-info" id='tabla_hoteles'>
                <thead>
                    <tr >
                        <th colspan="3" ></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody> 
                    <?php
                    $encuentra_hoteles = $select->consulta_hotele($_SESSION['municipio']);

                    while ($arreglo_encuentra_hoteles = mysqli_fetch_array($encuentra_hoteles)) {
                        ?>

                        <tr>
                            <td colspan="3">
        <?php echo $arreglo_encuentra_hoteles['nombre'] ?>
                            </td>
                            <td>
        <?php echo $arreglo_encuentra_hoteles ['ubicacion'] ?>
                            </td>
                            <td>
        <?php echo '$' . $arreglo_encuentra_hoteles ['precio'] ?>
                            </td>
                        </tr>
            <?php } ?>

                </tbody>
            </table>
    <?php
}
?>
    </div>

    <div class="col-md-5 ">
<?php
if (($_SESSION['numero_pregunta'] == 9 || $_SESSION['numero_pregunta'] == 10) && $activa_consulta == 1) {
    ?>
            <section class="column width4 first" >
                <div class="column width1 first">
                    <input type="hidden" id="start" value="<?php echo $_SESSION['origen'] ?>" placeholder="address or coordinates" />
                    <br/>
                    <input type="hidden" id="end" value="<?php echo $_SESSION['municipio'] ?>" placeholder="address or coordinates" />
                </div>
                <div class="column unitx1 align-center"></div>


                <br/>
                <div id="results" style="width: 990px; height: 500px;" class="column first">
                    <div id="map_canvas" style="width: 65%; height: 100%; float: left;"></div>
                    <div id="directions_panel" style="width: 35%; height: 100%; overflow: auto; float: right;"></div>
                </div> 

                <div class="text-right">

                    <select  id="travelMode" class="routeOptions" >
                        <option  value="DRIVING" selected="selected"></option>

                    </select>


                    <select id="unitSystem" class="routeOptions">
                        <option value="METRIC" selected="selected"></option>

                    </select>
                </div>

            </section>
    <?php
}
?>


        <script type='text/javascript'>
            var map = null;
            var directionsDisplay = null;
            var directionsService = null;

            function initialize() {
                var myLatlng = new google.maps.LatLng(20.68009, -101.35403);
                var myOptions = {
                    zoom: 4,
                    center: myLatlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                map = new google.maps.Map($("#map_canvas").get(0), myOptions);
                directionsDisplay = new google.maps.DirectionsRenderer();
                directionsService = new google.maps.DirectionsService();
            }

            function getDirections() {
                var start = $('#start').val();
                var end = $('#end').val();
                if (!start || !end) {
                    alert("Start and End addresses are required");
                    return;
                }
                var request = {
                    origin: start,
                    destination: end,
                    travelMode: google.maps.DirectionsTravelMode[$('#travelMode').val()],
                    unitSystem: google.maps.DirectionsUnitSystem[$('#unitSystem').val()],
                    provideRouteAlternatives: true
                };
                directionsService.route(request, function (response, status) {
                    if (status == google.maps.DirectionsStatus.OK) {
                        directionsDisplay.setMap(map);
                        directionsDisplay.setPanel($("#directions_panel").get(0));
                        directionsDisplay.setDirections(response);
                    } else {
                        alert("There is no directions available between these two points");
                    }
                });
            }

            $('#search').live('click', function () {
                getDirections();
            });
            $('.routeOptions').live('change', function () {
                getDirections();
            });

            $(document).ready(function () {
                initialize();
                gmaps_ads();
            });


        </script>

    </div>
<?php include("../_shared/footer.php"); ?>

