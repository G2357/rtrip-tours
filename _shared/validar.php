<!--
Nombre del archivo: validar.php
Autor original: Scrum team
Fecha de creacion de archivo:26 de marzo de 2016
Descripcion: Se crean las variables para poder guardar la informacion en el transcurso de la transaccion ademas de permitir el acceso al usuario
-->

<?php
//creacion de un obj del tipo class_login la clase que se encuentra en class_login y la cual realiza una consulta
//y regresa los datos de el usiario
$Logeado = new class_login();

//$Logeado->query("INSERT INTO `idiomas`.`usuarios` (`id_usu`, `nombre`, `contrasena`, `status`, `tipo`) VALUES ('0', '0', '0', b'1', '1');");
//chek que se envien los datos del alumno por el metodo post de las cajas de texto
if (isset($_POST['usuario'])) {


    $datos_login = $_POST['usuario'];


    $datos = $Logeado->get_usuario($datos_login);

    $row = mysqli_fetch_array($datos);

    if (isset($row)) {



        $_SESSION['usuario'] = $row['nombre'];
        $_SESSION['id_usuario'] = $row['id_usuario'];
        $_SESSION['contrasena'] = $row['contrasena'];


        $_SESSION['numero_pregunta'] = 1;
        $_SESSION['estado'] = "";
        $_SESSION['municipio'] = "";
        $_SESSION['sitio'] = "";
        $_SESSION['clima'] = "";
        $_SESSION['hotel'] = "";
        $_SESSION['restaurante'] = "";
        $_SESSION['comida'] = "";
        $_SESSION['origen'] = "";
        $_SESSION['limpiar'] = 0;
        $_SESSION['origen_estado'] = "";
        $_SESSION['aeropuerto'] = "";
        $_SESSION['paquete'] = "";
        
        $_SESSION['caso'] = "";
        $_SESSION['temporal'] = "";
        $_SESSION['actividad'] = "";
        $_SESSION['casifiacion_hotel'] = "";

        $borrar = $Logeado->borrar_chat();
        // $Logeado->actualizar();
        // $Logeado->actualizar_datos();
        //header("Location:index2.php");
        header("Location:views/chat.php");
    } else {
        //si no tiene datos row re dirigir a la misma pag pero con un dato en la id 

        header("Location:index.php?usuario=no_existe");
    }
}
?>