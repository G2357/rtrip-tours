<!----------------------------------------------------------------------------------------------------------
|Objetivo: Redactar  los valores de la empresa  y dar formato a la pagina de tal manera que tenga un grado |
de coherencia en cuanto a las metricas acordadas por el equipo                                             |
|Autor original: Diego Cervantes Garcia                                               Fecha: 06/03/2017    |                                                                         
----------------------------------------------------------------------------------------------------------->
<!DOCTYPE html>
<?php
//Se incluyen los archivos del cabezal, menu y pie de pagina
//include '/head.php';
include '/head.php';
?>

<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="../images/favicon.ico"> <!--Las librerias de metadatos deben de estar ordenados de como estan plasmados en las tres lineas anteriores-->
        <title>RTrip Tours | Valores </title>
        <!-- Bootstrap core CSS -->
        <link href="http://localhost/RTripTours/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="http://localhost/RTripTours/CSS/Estilos/cover.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script src="http://localhost/RTripTours/lib/bootstrap/js/bootstrap.min.js"></script>

        <link rel="icon" href="../../favicon.ico">

        <title>Valores</title>

        <!-- Bootstrap core CSS -->
        <link href="http://localhost/RTripTours/Librerias/bootstrap-3.3.6/docs/dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <link href="http://localhost/RTripTours/Librerias/bootstrap-3.3.6/docs/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="http://localhost/RTripTours/CSS/Estilos/cover.css" rel="stylesheet">

        <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
        <script src="http://localhost/RTripTours/Librerias/bootstrap-3.3.6/docs/assets/js/ie-emulation-modes-warning.js"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>

        <div class="site-wrapper">

            <div class="site-wrapper-inner4">

                <div class="cover-container">

                    <!--<div class="masthead clearfix">
                      <div class="inner">
                        <h3 class="masthead-brand">ViajAndo MX</h3>
                        <nav>
                          <ul class="nav masthead-nav">
                            <li class="active"><a href="#">Inicio</a></li>
                            <li><a href="#">Acerca de..</a></li>
                            <li><a href="#">Galeria</a></li>
                          </ul>
                        </nav>
                      </div>|
                    </div> -->

                    <font color="white"><div class="inner cover">
                        <h1 class="cover-heading ">Valores.</h1><!--en esta parte empezare a escribir las politicas 
                            
                            Nota: cada parrafo  tendra que separarse con  <p  </p>-->
                        <h3>Compromiso: Esforzarse para brindar el mejor servicio y atencion posible a nuestros clientes.;;
                        </h3>
                        <br>
                        <h3>Integridad: Relacionarse con los clientes y personal de una manera auténtica.
                        </h3> 
                        <br>
                        <h3>Calidad: De modo que nuestros servicios sean de excelencia.
                        </h3>  
                        <br>
                        <h3>Honestidad: Orientado tanto para los miembros de la empresa entre sí, como con los clientes. Se promueve la verdad como una herramienta elemental para generar confianza y la credibilidad de la empresa.
                        </h3>  
                        <br>
                        <h3>Confianza: Tener personal capacitado para poder brindar a nuestros clientes el mejor servicio con calidad, hospitalidad y puntualidad.
                        </h3>
                        <br>

                        <div>

                            <hr>
                        </div>

                    </div>
                    <div>
                        <p class="lead">
                            <a href="http://localhost/RTripTours/" class="btn btn-warning">Página Principal</a>
                        </p>
                    </div>
                </div>
                <div class="mastfoot">
                </div>
            </div>
        </div>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script src="http://localhost/RTripTours/Librerias/bootstrap-3.3.6/docs/dist/js/bootstrap.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="http://localhost/RTripTours/Librerias/bootstrap-3.3.6/docs/assets/js/ie10-viewport-bug-workaround.js"></script>
    </body>
</html>
<?php
include './footer.php';
?>