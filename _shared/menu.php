<?php
/* * ************************************************************************************************
 * Objetivo: Construir el carrusel de imagenes de la pagina principal que                           *
 * tiene como funcion proporcionar informacion visual al usuario                                    *
 * Autor:Diego Cervantes Garcia, Maria Esmeralda Espinosa Hernandez                Fecha: 10/03/2017*
 * **************************************************************************************************/
?>
<link rel="stylesheet" href="RTripTours\CSS\Estilos\cover.css" type="text/css" media="screen"/>
<br>
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>
    <div>
        <div> 
            <br>
            <br>
            <div class="carousel-inner" role="listbox">
                <div class="item active">

                    <div class="container">
                        <div class="carousel-caption">
                            <h1>Vive, Sueña... ya estás ahí.</h1>
                            <p>La compañia numero 1 en viajes</p>
                            <img src="images/Chiapa de Corzo - Chiapas.jpg" height="490" width="950" >

                            <p><br><a  btn-lg btn-success  class="btn btn-warning" href="http://localhost/RTripTours/views/mantenimiento.php" role="button">Saber más</a></p>
                        </div>
                    </div>
                </div>

                <!--en esta parte imagen 2-->

                <div class="item">

                    <div class="container">
                        <div class="carousel-caption">
                            <h1>Vive, Sueña... ya estás ahí.</h1>
                            <p>La compañia numero 1 en viajes</p>
                            <img src="images/Xilitla - San Luis Potosí.jpg" height="490" width="950" >

                            <p><br><a  btn-lg btn-success  class="btn btn-warning" href="http://localhost/RTripTours/views/mantenimiento.php" role="button">Saber más</a></p>
                        </div>
                    </div>
                </div>

                <!--en esta parte imagen 3-->
                <div class="item">

                    <div class="container">
                        <div class="carousel-caption">
                            <h1>Vive, Sueña... ya estás ahí.</h1>
                            <p>La compañia numero 1 en viajes</p>
                            <img src="images/tequisquiapan-queretaro.jpg" height="490" width="950" >

                            <p><br><a  btn-lg btn-success  class="btn btn-warning" href="http://localhost/RTripTours/views/mantenimiento.php" role="button">Saber más</a></p>
                        </div>
                    </div>
                </div>

                <!--en esta parte imagen 4-->
                <div class="item">

                    <div class="container">
                        <div class="carousel-caption">
                            <h1>Vive, Sueña... ya estás ahí.</h1>
                            <p>La compañia numero 1 en viajes</p>
                            <img src="images/Huamantla - Tlaxcala.jpg" height="490" width="950" >

                            <p><br><a  btn-lg btn-success  class="btn btn-warning" href="http://localhost/RTripTours/views/mantenimiento.php" role="button">Saber más</a></p>
                        </div>
                    </div>
                </div>


                <div class="item">

                    <div class="container">
                        <div class="carousel-caption">
                            <h1>El viaje de sus sueños</h1>
                            <p>esta a un clic de distancia</p>
                           <img src="images/Palizada - Campeche.jpg" height="490" width="950" >

                            <p><br><a  btn-lg btn-success class="btn btn-warning" href="http://localhost/RTripTours/views/mantenimiento.php" role="button">Saber más</a></p>
                        </div>
                    </div>
                </div>

            </div>
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Anterior</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Siguiente</span>

            </a>
        </div>
    
    </div><!-- /.carousel -->
    <div>
        <video style="weight 300;" controls="" whidth="500" heigth= "440" >
            <source src="http://localhost/RTripTours/video/Me.mp4" type="video/mp4"></video>
    </div>
    