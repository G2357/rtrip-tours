<!----------------------------------------------------------------------------------------------------------
|Objetivo: establecer informacion relevante de la agencia asi como del equipo de trabajo y dar formato a la|
pagina de tal manera que tenga un grado de coherencia en cuanto a las metricas acordadas por el equipo     |                                       |
|Autor original: Diego Cervantes Garcia                                               Fecha: 06/03/2017    |                                                                         
----------------------------------------------------------------------------------------------------------->
<!DOCTYPE html>
<?php
//Se incluyen los archivos del cabezal, menu y pie de pagina
//include '/head.php';
include '/head.php';
?>
<html lang="en">
  <head>
      <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="../images/favicon.ico"> <!--Las librerias de metadatos deben de estar ordenados de como estan plasmados en las tres lineas anteriores-->
        <title>RTrip Tours | Políticas </title>
        <!-- Bootstrap core CSS -->
        <link href="http://localhost/RTripTours/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="http://localhost/RTripTours/CSS/Estilos/cover.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
        <script src="http://localhost/RTripTours/lib/bootstrap/js/bootstrap.min.js"></script>
    
    

    <title>Acerca de..</title>

    <!-- Bootstrap core CSS -->
    <link href="http://127.0.0.1/RTripTours/Librerias/bootstrap-3.3.6/docs/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="http://127.0.0.1/RTripTours/Librerias/bootstrap-3.3.6/docs/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="http://127.0.0.1/RTripTours/CSS/Estilos/cover.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="http://127.0.0.1/RTripTours/Librerias/bootstrap-3.3.6/docs/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="site-wrapper">

      <div class="site-wrapper-inner4">

        <div class="cover-container">


          <font color="white"><div class="inner cover">
            <h1 class="cover-heading">Acerca de...</h1>
            <p class="lead">
                <br>
                Creado por RTripTours
                <br>
                
                
            </p>
            <p class="lead">
              <a href="http://127.0.0.1/RTripTours/" class="btn btn-lg btn-default">Página Principal</a>
            </p>
          </div>

        

        </div>

      </div>

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="http://127.0.0.1/RTripTours/Librerias/bootstrap-3.3.6/docs/dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="http://127.0.0.1/RTripTours/Librerias/bootstrap-3.3.6/docs/assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
<?php
include './footer.php';
?>